;;; -*- indent-tabs-mode:nil; -*-

;;; Copyright (c) 2014, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.


(in-package :asdf-user)

(asdf:defsystem #:temporal-networks-tests
  :name "Temporal Networks Tests"
  :version "1.8.0"
  :description "Tests for temporal networks package."
  :author "Eric Timmons"
  :maintainer "MIT MERS Group"
  :pathname "tests"
  :serial t
  :components ((:file "package")
               (:file "test-suite")
               (:file "test-events")
               (:file "test-constraints"))
  :depends-on (#:temporal-networks
               #:float-features
               #:fiveam))
