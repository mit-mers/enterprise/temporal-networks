;;; -*- indent-tabs-mode:nil; -*-

;;; Copyright (c) 2014, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.


(in-package :asdf-user)

(asdf:defsystem #:temporal-networks
  :name "Temporal Networks"
  :version (:read-file-form "version.lisp-expr")
  :description "This package contains the definitions of data
structures used to specify temporal networks, including simple
temporal networks, and methods used to manipulate and read/write the
networks."
  :author "Eric Timmons"
  :maintainer "MIT MERS Group"
  :pathname "src"
  :serial t
  :components ((:file "package")
               (:file "infinity")
               (:file "features")
               (:file "temporal-networks")
               (:file "hooks")
               (:file "networks" :depends-on ("features"))
               (:file "constraints" :depends-on ("networks"))
               (:file "events" :depends-on ("networks"))
               (:file "conditions" :depends-on ("networks"))
               (:file "copying" :depends-on ("networks" "constraints"))
               (:file "conflicts")
               (:file "probability-shim")
               (:module "serialization"
                        :serial t
                        :components ((:file "mtk-serialization")
                                     (:file "xml")
                                     (:file "dot")))
               (:file "swank-extension" :if-feature :swank))
  :in-order-to ((test-op (load-op :temporal-networks-tests)))
  :perform (test-op :after (op c)
                    (funcall (read-from-string "fiveam:run!") :temporal-networks))
  :depends-on (#:parse-number
               #:s-xml
               #:log4cl
               #:trivial-garbage
               #:cl-ppcre
               #:cl-dot
               #:alexandria
               #:float-features))

(asdf:defsystem #:temporal-networks/constraint-system
  :name "Temporal Networks Constraint System Wrapper"
  :version (:read-file-form "version.lisp-expr")
  :description "Makes temporal networks behave like a constraint system usable
by OpSAT."
  :author "Eric Timmons"
  :maintainer "MIT MERS Group"
  :pathname "src/constraint-system"
  :serial t
  :components ((:file "package")
               (:file "constraint-system")
               (:file "bimap")
               (:file "relaxable-constraint")
               (:file "continuous-temporal-relaxation")
               (:file "compute-continuous-relaxations"))
  :depends-on (#:temporal-networks
               #:chance-constraint
               #:constraint-system
               #:temporal-controllability
               #:probability-distribution
               #:num-opt))
