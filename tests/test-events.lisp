;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks-tests)

(in-suite :temporal-networks)

(def-test ensure-event-creates-events (:fixture stn)
  (is (eq (tn::ensure-event! *stn* :e1)
          (tn::ensure-event! *stn* :e1)))
  (is (eq (tn::ensure-event! *stn* :e2)
          (find-event *stn* :e2))))

(def-test event-hooks (:fixture stn)
  (flet ((my-hook (network event)
           (is (eql :e1 (id event)))
           (is (eql *stn* network))
           (error 'hook-fired)))
    (let ((cb (add-event-added-handler *stn* #'my-hook)))
      (signals hook-fired
        (tn::ensure-event! *stn* :e1))
      (finishes
        (tn::ensure-event! *stn* :e1)
        (remove-event-added-handler *stn* cb)
        (tn::ensure-event! *stn* :e2)))))
