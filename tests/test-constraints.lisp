;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks-tests)

(in-suite :temporal-networks)

(def-test event-resolution-by-constraints (:fixture stn)
  ;; Make an instance of a constraint. Ensure that it makes two events
  ;; and registers itself appropriately.
  (let* ((tc (make-instance 'simple-temporal-constraint :from-event :e1 :to-event :e2 :network *stn*))
         (e1 (find-event *stn* :e1))
         (e2 (find-event *stn* :e2))
         (tc2 (make-instance 'simple-temporal-constraint :from-event e1 :to-event e2 :network *stn*)))
    (is (eq e1
            (from-event tc)))
    (is (eq e2
            (to-event tc)))
    (is (member tc (outgoing-constraints e1)))
    (is (member tc (incoming-constraints e2)))
    ;; Check second constraint.
    (is (eq e1
            (from-event tc2)))
    (is (eq e2
            (to-event tc2)))
    (is (member tc2 (outgoing-constraints e1)))
    (is (member tc2 (incoming-constraints e2)))))

(def-test error-on-out-of-network-event-resolution-by-constraints (:fixture stn)
  ;; This test ensures that make a constraint as part of stn1 using
  ;; events from stn2 signals a condition.
  (with-test-stn (:name stn2)
    (let ((e1-2 (make-instance 'temporal-event :network stn2 :id :e1-2))
          (e2-2 (make-instance 'temporal-event :network stn2 :id :e2-2)))
      (signals mismatching-networks
        (make-instance 'simple-temporal-constraint :from-event e1-2 :to-event :e2 :network *stn*))
      (signals mismatching-networks
        (make-instance 'simple-temporal-constraint :from-event :e1 :to-event e2-2 :network *stn*))
      (signals mismatching-networks
        (make-instance 'simple-temporal-constraint :from-event e1-2 :to-event e2-2 :network *stn*)))))
