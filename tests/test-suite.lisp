;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks-tests)

(def-suite :temporal-networks :description "Test suite for the temporal networks package.")
(in-suite :temporal-networks)

(defvar *stn* nil "When using an STN test fixture, this is bound to the STN object.")

(define-condition hook-fired ()
  ())

;; Testing macro
(defmacro with-test-stn ((&key name) &body body)
  (unless name
    (setf name '*stn*))
  `(let ((,name (make-simple-temporal-network (gensym "TEST-FIXTURE-STN"))))
     ,@body))

;;; Make a few useful fixtures.

(def-fixture stn ()
  (with-test-stn ()
    (&body)))
