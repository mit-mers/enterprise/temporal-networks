## v1.9.2
Date: March 30, 2022

Same as v1.9.0, but with corrections for CI processes.

### Added

- Nothing

### Removed

- Nothing

## v1.9.0 - March 28, 2022

### Removed

- `mtk-utils` dependencies. Replaced `mtk-utils-infinity` with `float-features:double-float-{positive|negative}-infinity`
- Local mtk dependencies in lieu of CLPI dependencies

## v1.8.0 - July 21, 2020

### Added

- CLPM support
- CI jobs for testing on SBCL
