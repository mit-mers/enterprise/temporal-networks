;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Finalizers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *finalizers* (tg:make-weak-hash-table :test #'eq :weakness :key))

(defstruct finalizer-sentinel
  "Contains a list of `event-handlers` objects to which has listeners
registered."
  (list
   nil
   :type list))

(defun ensure-finalizer-sentinel (obj)
  (let ((sentinel (gethash obj *finalizers*)))
    (unless sentinel
      (setf sentinel (make-finalizer-sentinel))
      (setf (gethash obj *finalizers*) sentinel)
      (let ((obj-weak-ptr (tg:make-weak-pointer obj)))
        (tg:finalize obj (lambda ()
                           (let ((obj-strong-ptr (tg:weak-pointer-value obj-weak-ptr)))
                             (mapcar (lambda (x)
                                       (remove-handlers-by-source x
                                                                  obj-strong-ptr))
                                     (finalizer-sentinel-list sentinel)))))))
    sentinel))

(defun add-finalizer (obj event-handlers)
  (push event-handlers (finalizer-sentinel-list (ensure-finalizer-sentinel obj))))

(defun remove-finalizer (obj event-handlers)
  (let ((sentinel (ensure-finalizer-sentinel obj)))
    (setf (finalizer-sentinel-list sentinel)
          (remove event-handlers (finalizer-sentinel-list sentinel) :test #'eq))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Dealing with object references
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(declaim (inline make-weak-ptr))
(defun make-weak-ptr (object-ref)
  (trivial-garbage:make-weak-pointer object-ref))

;; Make this never be called inline so that it doesn't accidentally
;; close over the strong pointer to the object...
(declaim (notinline make-weak-callback))
(defun make-weak-callback (cb weak-object-ref)
  #'(lambda (&rest x) (let ((obj (trivial-garbage:weak-pointer-value weak-object-ref)))
                        (when obj
                          (apply cb obj x)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Data structure that holds event handlers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct event-handlers
  (handlers-by-source (tg:make-weak-hash-table :weakness :key) :read-only t))

(declaim (inline add-handler))
(defun add-handler (event-handlers handler &key source provide-source-to-handler?)
  (let* ((source-weak-ptr nil)
         (handler-object nil))
    (when source
      (setf source-weak-ptr (make-weak-ptr source))
      (add-finalizer source event-handlers))
    (when (and source provide-source-to-handler?)
      ;; When we're given a source for this handler and asked to provide the
      ;; source to the handler, make a handler that dereferences the
      ;; pointer.
      (setf handler (make-weak-callback handler source-weak-ptr)))

    ;; The handler object is a cons cell with the handler and the source weak
    ;; ptr.
    (setf handler-object (cons handler source-weak-ptr))

    ;; Add it to the hashtable, registering it with its (strong) source.
    (push handler-object (gethash source (event-handlers-handlers-by-source event-handlers)))
    handler-object))

(declaim (inline remove-handler))
(defun remove-handler (event-handlers handler)
  "Remove an event added handler. HANDLER must be a reference returned
by ADD-HANDLER."

  (let ((handlers-by-source (event-handlers-handlers-by-source event-handlers))
        (source))
    (when (cdr handler)
      (setf source (tg:weak-pointer-value (cdr handler))))
    (setf (gethash source handlers-by-source)
          (delete handler (gethash source handlers-by-source) :count 1 :test #'eq))
    (when (and source
               (null (gethash source handlers-by-source)))
      (remhash source handlers-by-source)
      (remove-finalizer source event-handlers)))
  (values))

(declaim (inline fire-handlers))
(defun fire-handlers (event-handlers &rest args)
  (maphash (lambda (k v)
             (declare (ignore k))
             (dolist (handler-object v)
               (apply (car handler-object) args)))
           (event-handlers-handlers-by-source event-handlers)))

(declaim (inline clear-handlers))
(defun clear-handlers (event-handlers)
  (maphash (lambda (k v)
             (declare (ignore v))
             (when k
               (remove-finalizer k event-handlers)))
           (event-handlers-handlers-by-source event-handlers))
  (clrhash (event-handlers-handlers-by-source event-handlers)))

(declaim (inline remove-handlers-by-source))
(defun remove-handlers-by-source (event-handlers source)
  (remove-finalizer source event-handlers)
  (remhash source (event-handlers-handlers-by-source event-handlers)))
