;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BASE CLASS TEMPORAL-NETWORK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftype temporal-network-domain-t ()
  '(member :continuous :discrete))

(defclass temporal-network ()
  ((name
    :initarg :name
    :initform (string (gensym))
    :reader name
    :documentation "The name of this network.")
   (time-domain
    :initarg :time-domain
    :reader time-domain
    :initform :continuous
    :type temporal-network-domain-t
    :documentation
"A keyword, either :CONTINUOUS or :DISCRETE. Determines if the variables in this
temporal constraint network (the events) have a discrete or continuous
domain. Defaults to :CONTINUOUS.")
   (features
    :initarg :features
    :reader features
    :type list
    :initform (error "FEATURES must be provided for a TEMPORAL-NETWORK.")
    :documentation
"The features that this temporal constraint network supports. The current full
list of features are:

+ :SIMPLE-TEMPORAL-CONSTRAINTS
+ :CONTINGENT-TEMPORAL-CONSTRAINTS
+ :SIMPLE-CONTINGENT-TEMPORAL-CONSTRAINTS")

   ;; Events
   (events-by-id
    :initform (make-hash-table :test #'equal)
    :type hash-table
    :reader events-by-id
    :documentation "A hash table mapping event IDs (symbols) to event objects.")
   (default-event-type
    :initform 'temporal-event
    :initarg :default-event-type
    :reader default-event-type
    :documentation
"The class name that is used as default when instantiating events for this
network.")
   (event-added-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :accessor event-added-handlers
    :documentation
"These handlers are called when an event is added to this temporal network.")

   ;; Constraints.
   (constraints-by-id
    :initform (make-hash-table :test #'equal)
    :type hash-table
    :reader constraints-by-id
    :documentation
"A hash table mapping constraint IDs (symbols) to constraint objects.")
   (constraint-added-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :accessor constraint-added-handlers
    :documentation
"These handlers are called when a constraint is added to this temporal
network.")
   (constraint-removed-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :accessor constraint-removed-handlers
    :documentation
"These handlers are called when a constraint is removed from this temporal
network.")

   ;; Consistency Checking
   (consistency-checker
    :initform nil
    :reader default-consistency-checker
    :writer %default-consistency-checker
    :documentation "A slot to hold the default consistency checker for
this network."))
  (:documentation
"A temporal network, consisting of events and constraints between the events
dictating the relationship between the points in time at which the events must
occur."))

(defmethod initialize-instance :after ((network temporal-network) &rest initargs)
  (declare (ignore initargs))
  ;; Make implicit features explicit.
  (setf (slot-value network 'features)
        (resolve-feature-dependencies (slot-value network 'features))))

(defmethod (setf default-consistency-checker) ((new-value symbol) network &rest initargs)
  "If the NEW-VALUE is a symbol, call make instance on it and pass the initargs
to the constructor."
  (declare (type temporal-network network))
  (call-next-method (apply #'make-instance new-value :temporal-network network initargs) network))

(defmethod (setf default-consistency-checker) (new-value network &rest initargs)
  (declare (ignore initargs)
           (type temporal-network network))
  (%default-consistency-checker new-value network))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Helper Classes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass temporal-network-member ()
  ((name
    :initarg :name
    :reader name
    :type string
    :documentation "The name of this object. Name must be a string.")
   (id
    :initarg :id
    :reader id
    :type symbol
    :initform (gensym)
    :documentation
"The ID of this object. Must be a symbol and unique within the network this
event is associated with. If it is initialized to NIL, an ID will be
generated.")
   (network
    :reader network
    :initarg :network
    :type (or null temporal-network)
    :initform nil)
   (source
    :reader source
    :initarg :source
    :initform nil))
  (:documentation "Mixin that indicates some class is meant to be
associated with a temporal network.

If a network is provided upon instantiation, the object is immediately added to
the network. Otherwise, no guarantees are made that references exist or the ID
is unique.

If an ID is not provided, a unique one is generated. If a name is not provided,
the name is generated from the ID.

After instances of this class with a provided network are initialized, the
object is added to the associated network. Errors will be signalled if the ID is
not unique. Additionally, hooks will be fired to let listeners know that the
object was added to the network."))

(defmethod initialize-instance :around ((tn-member temporal-network-member) &rest initargs)
  "This method ensures that ADD-OBJECT-TO-NETWORK is called."
  (declare (ignore initargs))
  (call-next-method)
  ;; If name was not provided, set it equal to a string version of ID.
  (unless (slot-boundp tn-member 'name)
    (setf (slot-value tn-member 'name) (format nil "~A" (id tn-member))))
  ;; If a network was provided, add the object to that network.
  (when (network tn-member)
    (add-object-to-network! (network tn-member) tn-member))
  tn-member)

(defun add-object-to-network! (network object)
  "Given an object that is an instance of TEMPORAL-NETWORK-MEMBER that
is fully instantiated, first ensure the network supports the object,
then check it is unique, then call internal methods to add the object,
then call external methods to add the object. Last, fire hooks."
  (check-network-supports-object network object)
  (check-object-unique network object)
  (perform-add-object-to-network! network object)
  (process-object-added-hooks network object))

(defmethod perform-add-object-to-network! :before ((network temporal-network) (object temporal-network-member))
  (setf (slot-value object 'network) network))

(defun remove-object-from-network! (network object)
  ;; Ensure that OBJECT belongs to NETWORK
  (when (eql (network object) network)
    (multiple-value-prog1
        ;; Remove the object.
        (perform-remove-object-from-network! network object)
      ;; Process the handlers.
      (process-object-removed-hooks network object))))

(defmethod perform-remove-object-from-network! (network (object temporal-network-member))
  (declare (ignore network))
  ;; Set the NETWORK slot to NIL.
  (setf (slot-value object 'network) nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Consistency Checking
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod temporal-network-consistent? ((network temporal-network) &rest args)
  "Call method again on default consistency checker."
  (apply #'temporal-network-consistent? (default-consistency-checker network) args))

(defmethod temporal-network-consistent-with-chance-constraint?
           ((network temporal-network) chance-constraint &rest args)
  "Call method again on default consistency checker."
  (apply #'temporal-network-consistent-with-chance-constraint?
         (default-consistency-checker network) chance-constraint args))

(defmethod reset-consistency-checker! ((network temporal-network) &rest args)
  "Call method again on default consistency checker."
  (apply #'reset-consistency-checker! (default-consistency-checker network) args))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; STN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *stn-features* (list :simple-temporal-constraints))

(defun make-simple-temporal-network (name)
  "=> TEMPORAL-NETWORK instance

Convenience method to construct a temporal network that only supports simple
temporal constraints."
  (make-instance 'temporal-network :name name :features *stn-features*))

(defparameter *stnu-features* (list :simple-temporal-constraints :simple-contingent-temporal-constraints))
(defun make-simple-temporal-network-with-uncertainty (name)
  "=> TEMPORAL-NETWORK instance

Convenience method to construct a STNU"
  (make-instance 'temporal-network :name name :features *stnu-features*))

(defparameter *pstn-features* (list :simple-temporal-constraints :probabilistic-temporal-constraints))
(defun make-probabilistic-simple-temporal-network (name)
  "=> TEMPORAL-NETWORK instance

Convenience method to construct a pSTN"
  (make-instance 'temporal-network :name name :features *pstn-features*))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro do-events ((event-var network &optional resultform) &body body)
  (let ((function-name (gensym))
        (key-name (gensym))
        (value-name (gensym)))
    `(flet ((,function-name (,key-name ,value-name)
              (declare (ignore ,key-name))
              (let ((,event-var ,value-name))
                ,@body)))
       (maphash #',function-name (events-by-id ,network))
       ,resultform)))

(defmacro do-constraints ((var network &optional resultform) &body body)
  (let ((function-name (gentemp)))
    `(flet ((,function-name (k v)
              (declare (ignore k))
              (let ((,var v))
                ,@body)))
       (maphash #',function-name (constraints-by-id ,network))
       ,resultform)))
