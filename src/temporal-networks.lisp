;;; -*- indent-tabs-mode:nil; fill-column:80; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

;;; This file contains the generic function definitions that one must
;;; be aware of to use or extend the temporal networks package.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Callable
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Callable and Extendable
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric temporal-network-consistent? (network-or-consistency-checker &key &allow-other-keys)
  (:documentation
"=> (values consistent? temporal-conflict &optional checker-dependent-returns)

When called on a temporal network or a consistency checker (associated with a
network), determines if the network is consistent. If the network is
inconsistent, returns a summary of why the network is inconsistent.

When called on a temporal network, the call is simply passed to the default
consistency checker registered with the network (by
SET-DEFAULT-CONSISTENCY-CHECKER). All keyword arguments are simply passed
through directly.

The TEMPORAL-CONFLICT return value must be a list of constraints involved in
the conflict. Each element of the list must be a list where the first element is
a temporal constraint object and the remaining elements of the list specify
which part(s) of the constraint are involved in the conflict (i.e. :lower-bound
or :upper-bound)."))

(defgeneric temporal-network-consistent-with-chance-constraint?
            (network-or-consistency-checker chance-constraint
             &key &allow-other-keys)
  (:documentation
"=> (values consistent? temporal-conflict &optional checker-dependent-returns)

This method is analogous to TEMPORAL-NETWORK-CONSISTENT?, except that
it operates on a pSTN and requires a chance constraint to verify
the pSTN against.

As in TEMPORAL-NETWORK-CONSISTENT?, the TEMPORAL-CONFLICT return value
is a list of constraints. However, it contains not only temporal constraints,
but also the original chance constraint, because the conflict should express
what degree of consistency could not be met by the temporal constraints.
The first element of the conflict specifies the chance constraint, which
like each temporal constraint, is the first element in its own list.
The chance constraint is also the only element, because it contributes to
the conflict in its entirety, so there are no additional parts to specify."))

(defgeneric reset-consistency-checker! (network-or-consistency-checker &key hard)
  (:documentation
"Resets all internal state of the consistency checker. If called on a network,
this method is re-called on the default consistency checker. A condition is
signaled if no default consistency checker is registered.

If HARD is T, the effect of this method MUST be functionally equivalent to
re-instantiating the consistency checker with the exact same arguments with
which it was originally instantiated. If HARD is NIL, the consistency checker
may opt to keep some learned data around.

Additional keywords may be added in the future. Specializations of this method
should use &allow-other-keys to guard against this."))

(defgeneric (setf default-consistency-checker) (type-or-instance network &rest initargs)
  (:documentation "=> consistency checker

If TYPE-OR-INSTANCE is a symbol, instantiates a consistency checker of type
TYPE-OR-INSTANCE using INITARGS and sets it as the default consistency checker
of this network.

Otherwise, instance is set as the default consistency checker for the
network. If instance is NIL, that is interpreted as removing the current default
consistency checker (if one exists)."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Extensible functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *network-supports-object-condition-type* nil)
(defvar *network-supports-object-condition-initargs* nil)

(defgeneric network-supports-object? (network object)
  (:documentation
"=> (values object-supported? condition-initargs condition-type)

Determine if OBJECT can be successfully added to NETWORK. This is called by
CHECK-NETWORK-SUPPORTS-OBJECT. The primary methods must return a single value, a
generalized boolean indicating if the object can be added. If the primary
methods wish to influence the type of condition raised by
CHECK-NETWORK-SUPPORTS-OBJECT, they may set the variables
*NETWORK-SUPPORTS-OBJECT-CONDITION-TYPE* and
*NETWORK-SUPPORTS-OBJECT-CONDITION-INITARGS*. These variables have a new binding
created for them by the :around method.

If set, the condition type must be a subclass of UNSUPPORTED-OBJECT. :network
and :object are automatically added to the initargs.")
  (:method-combination and)
  (:method :around (network object)
           (let* ((*network-supports-object-condition-type* 'unsupported-object)
                  (*network-supports-object-condition-initargs* nil)
                  (supports? (call-next-method)))
             (if supports?
                 supports?
                 (progn
                   (setf (getf *network-supports-object-condition-initargs* :network) network)
                   (setf (getf *network-supports-object-condition-initargs* :object) object)
                   (values supports?
                           *network-supports-object-condition-initargs*
                           *network-supports-object-condition-type*))))))

(defgeneric check-network-supports-object (network object)
  (:documentation
"Calls NETWORK-SUPPORTS-OBJECT? to determine if OBJECT can be added to NETWORK.
If it cannot be added, a condition is signaled using the second and third return
values of NETWORK-SUPPORTS-OBJECT?.

This function can be extended to provide condition handlers if desired.")
  (:method (network object)
    ;; Default implementation that signals an error if network-supports-object?
    ;; returns nil.
    (multiple-value-bind (object-supported? condition-initargs condition-type)
        (network-supports-object? network object)
      (unless object-supported?
        (let ((condition-type (or condition-type 'unsupported-object)))
          (apply #'error condition-type condition-initargs))))))

(defgeneric check-object-unique (network object)
  (:documentation
"Given an object, check that it is unique within the network.

Must raise an error if the object is not unique."))

(defgeneric perform-add-object-to-network! (network object)
  (:documentation
"This function is used to perform the task of adding an object to a temporal
network. It is called internally and is provided only as an extension point. It
is not to be called directly. When this method is called, the object being added
is guaranteed to be fully instantiated, unique within the network, and supported
by the network.

A typical use of this function is to ensure consistency by updating references
in other objects in the network. For example, if a list of constraints that meet
a certain criteria needs to be kept, this would be the place to ensure they are
on that list.

All basic setup (ensuring constraints and events are added to the base
TEMPORAL-NETWORK data structure and events are updated with newly added
constraints) is done in the primary method of this function, so specializations
must call (CALL-NEXT-METHOD) or otherwise ensure those basic tasks are done.

If this function signals an unhandled condition, the network may be left in an
inconsistent state."))

(defgeneric perform-remove-object-from-network! (network object)
  (:documentation
"The return value of this function should not be relied on and may change in the
future.

This extension point is called to perform the work necessary to remove an object
from a temporal network. When this generic function returns, OBJECT must be
unreachable from NETWORK.

This function should NOT be called directly to remove an object. It is meant as
an extension point for the temporal-networks package.

If this function signals an unhandled condition, the network may be left in an
inconsistent state."))

(defgeneric process-object-removed-hooks (network object)
  (:documentation
"This is called once an object has been completely removed from the
network. This should fire all applicable hooks. The hooks provided by the
temporal-networks system are fired in this method, so specializations must
call (CALL-NEXT-METHOD) or otherwise ensure those hooks are fired."))

(defgeneric process-object-added-hooks (network object)
  (:documentation
"This is called once an object has been completely added to the network. This
should fire all applicable hooks. The hooks provided by the temporal-networks
system are fired in this method, so specializations must call (CALL-NEXT-METHOD)
or otherwise ensure those hooks are fired.")
  (:method ((network t) (object t))
    "The default method is to not call any hooks."
    (values)))

