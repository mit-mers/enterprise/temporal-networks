;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David C. Wang

(in-package #:tn/cs)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Class BIMAP
;; This class provides a very simple bidirectional mapping.
;; Two separate hash tables are used.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass bimap ()
  ((fwd
    :accessor bimap-fwd
    :initarg :fwd
    :initform nil
    :documentation "A hashtable that stores entries in the forward direction.")
   (bwd
    :accessor bimap-bwd
    :initarg :bwd
    :initform nil
    :documentation "A hashtable that stores entries in the backward direction.")))

(defmethod initialize-instance :after ((map bimap) &key (fwd-test #'eql) (bwd-test #'eql))
  (setf (slot-value map 'fwd) (make-hash-table :test fwd-test))
  (setf (slot-value map 'bwd) (make-hash-table :test bwd-test)))

(defun make-bimap (&key (fwd-test #'eql) (bwd-test #'eql))
  (make-instance 'bimap :fwd-test fwd-test :bwd-test bwd-test))

(defmacro with-bimap-slots (map reverse &rest body)
  `(if ,reverse
       (with-slots ((fwd bwd)(bwd fwd)) ,map (progn ,@body))
       (with-slots (fwd bwd) ,map (progn ,@body))))

(defgeneric map-add! (map key value &key replace reverse default &allow-other-keys)
  (:documentation "Adds the KEY VALUE pair to the MAP.
   Returns three values. The first value is boolean and represents if the KEY VALUE
   pair was added. The second value is the original value associated with the KEY,
   or DEFAULT if there was no originally associated value. The third value is the original
   key associated with the VALUE or DEFAULT if there was no originally associated KEY.
   If REPLACE is t (default), the new KEY-VALUE pair will replace the old key-value pair.
   If REPLACE is nil, the new KEY VALUE pair will only be added if there is no matching
   key or value in either of the directional maps.
   If REVERSE is nil (default), the KEY and VALUE arguments will be interpreted as key
   and value. If REVERSE is t, the KEY will be interpreted as the value,
   and the VALUE as the key."))

(defmethod map-add! ((map bimap)(key t)(value t) &key (replace t)(reverse nil)(default nil) &allow-other-keys)
  ""

  (with-bimap-slots
      map reverse
      (multiple-value-bind (fwd-value fwd-there?)
          (gethash key fwd default)
        (multiple-value-bind (bwd-value bwd-there?)
            (gethash value bwd default)
          (cond
            ;; if replace is true or there is not an existing fwd or bwd pair.
            ((or replace (and (not fwd-there?) (not bwd-there?)))
             ;; do the replacement
             (setf (gethash key   fwd) value)
             (setf (gethash value bwd) key)
             ;; return the values
             (values t fwd-value bwd-value))
            ;; otherwise, do not do the replacement
            (:otherwise
             (values nil fwd-value bwd-value)))))))

(defgeneric map-remove! (map key &key reverse default &allow-other-keys)
  (:documentation "Removes the KEY and associated value from the MAP.
   Returns two values.
     If the KEY is found, the first value will be the value
   associated with the key, the second value will be t,
   indicating something was removed.
     If the KEY is not found, the first value will be DEFAULT
   the second value will be nil."))

(defmethod map-remove! ((map bimap) (key t) &key (reverse nil) (default nil) &allow-other-keys)
  ""
  (with-bimap-slots
      map reverse
      (multiple-value-bind (value there?)
          (gethash key fwd)
        (if there?
            (progn (remhash key fwd) (values value t))
            (values default nil)))))

(defgeneric map-get (map key &key reverse default &allow-other-keys)
  (:documentation   "Returns the value associated with KEY in the MAP.
   Returns two values.
     If the KEY is found, the first value will be the value
   associated with the key, the second value will be t,
   indicating they key was found.
     If the KEY is not found, the first value will be DEFAULT
   the second value will be nil."))


(defmethod map-get ((map bimap)(key t) &key (reverse nil) (default nil) &allow-other-keys)
  ""
  (with-bimap-slots
      map reverse
      (gethash key fwd default)))

(defmethod map-map ((map bimap) (f function) &key (reverse nil) &allow-other-keys)
  "Applies a function F to each entry in the MAP.
   The function is required to take two arguments - the key and the value."
  (if reverse
      (maphash f (bimap-bwd map))
      (maphash f (bimap-fwd map))))
