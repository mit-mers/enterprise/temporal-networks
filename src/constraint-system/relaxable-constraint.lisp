;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.


(in-package :tn/cs)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Resource Constraint Definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass relaxable-simple-temporal-constraint (simple-temporal-constraint)

  ((lb-relaxable?
    :initarg :lb-relaxable?
    :accessor stc-lb-relaxable?
    :initform nil
    :documentation
    "Indicates if the lower bound can be relaxed.")

   (lb-cost-ratio
    :initarg :lb-cost-ratio
    :accessor stc-lb-cost-ratio
    :initform nil
    :documentation
    "A linear function gradient: indicates the ratio
     between cost and the relaxation of lb.")

   (lb-relaxed?
    :initarg :lb-relaxed?
    :accessor stc-lb-relaxed?
    :initform nil
    :documentation
    "Indicates if the lower bound has been relaxed.")

   (original-lb
    :initarg :original-lb
    :accessor stc-original-lb
    :initform nil
    :documentation
    "Stores the original value of lb.")

   (ub-relaxable?
    :initarg :ub-relaxable?
    :accessor stc-ub-relaxable?
    :initform nil
    :documentation
    "Indicates if the upper bound can be relaxed.")

   (ub-cost-ratio
    :initarg :ub-cost-ratio
    :accessor stc-ub-cost-ratio
    :initform nil
    :documentation
    "A linear function gradient: indicates the ratio
     between cost and the relaxation of ub.")

   (ub-relaxed?
    :initarg :ub-relaxed?
    :accessor stc-ub-relaxed?
    :initform nil
    :documentation
    "Indicates if the upper bound has been relaxed.")

   (original-ub
    :initarg :original-ub
    :accessor stc-original-ub
    :initform nil
    :documentation
    "Stores the original value of ub."))

  (:documentation "This is the base object represents a relaxable temporal constraint. It subclass the simple temporal constraint class."))

(defclass relaxable-simple-contingent-temporal-constraint (simple-contingent-temporal-constraint)

  ((lb-relaxable?
    :initarg :lb-relaxable?
    :accessor utc-lb-relaxable?
    :initform nil
    :documentation
    "Indicates if the lower bound can be relaxed.")

   (lb-cost-ratio
    :initarg :lb-cost-ratio
    :accessor utc-lb-cost-ratio
    :initform nil
    :documentation
    "A linear function gradient: indicates the ratio
     between cost and the relaxation of lb.")

   (lb-relaxed?
    :initarg :lb-relaxed?
    :accessor utc-lb-relaxed?
    :initform nil
    :documentation
    "Indicates if the lower bound has been relaxed.")

   (original-lb
    :initarg :original-lb
    :accessor utc-original-lb
    :initform nil
    :documentation
    "Stores the original value of lb.")

   (ub-relaxable?
    :initarg :ub-relaxable?
    :accessor utc-ub-relaxable?
    :initform nil
    :documentation
    "Indicates if the upper bound can be relaxed.")

   (ub-cost-ratio
    :initarg :ub-cost-ratio
    :accessor utc-ub-cost-ratio
    :initform nil
    :documentation
    "A linear function gradient: indicates the ratio
     between cost and the relaxation of ub.")

   (ub-relaxed?
    :initarg :ub-relaxed?
    :accessor utc-ub-relaxed?
    :initform nil
    :documentation
    "Indicates if the upper bound has been relaxed.")

   (original-ub
    :initarg :original-ub
    :accessor utc-original-ub
    :initform nil
    :documentation
    "Stores the original value of ub."))

  (:documentation "This is the base object represents a relaxable contingent temporal constraint. It subclass the simple contingent temporal constraint class."))


(defmethod network-supports-object? and (network (constraint relaxable-simple-temporal-constraint))
  "Relaxable simple temporal constraints require :SIMPLE-TEMPORAL-CONSTRAINTS on the
network's features list."
  (member :simple-temporal-constraints (tn:features network)))

(defmethod network-supports-object? and (network (constraint relaxable-simple-contingent-temporal-constraint))
  "Relaxable simple contingent temporal constraints require :SIMPLE-CONTINGENT-TEMPORAL-CONSTRAINTS on the
network's features list."
  (member :simple-contingent-temporal-constraints (tn:features network)))
