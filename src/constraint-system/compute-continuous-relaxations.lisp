;;; -*- Mode:Common-Lisp; Package:CS; Base:10 -*-

;;; Copyright (c) 1999 - 2016 Brian C. Williams, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :tn/cs)

(defgeneric apply-relaxation (relaxation)
  (:documentation "This function applies the relaxation to the constraint."))

(defgeneric undo-relaxation (relaxation)
  (:documentation "This function undo the relaxation to the constraint."))

(defmethod apply-relaxation ((relaxation continuous-temporal-relaxation))

  "Apply the relaxed bounds of this relaxation to its stc/utc"

  (let* ((constraint (ctr-temporal-constraint relaxation))
         (relaxing-lb? (ctr-relaxing-lb? relaxation))
         (relaxed-lb (ctr-relaxed-lb relaxation))
         (relaxing-ub? (ctr-relaxing-ub? relaxation))
         (relaxed-ub (ctr-relaxed-ub relaxation)))

    (ecase (type-of constraint)
      (relaxable-simple-temporal-constraint
       (when relaxing-lb?
         (setf (stc-lb-relaxed? constraint) t)
         (setf (stc-original-lb constraint) (lower-bound constraint))
         (setf (lower-bound constraint) relaxed-lb))
       (when relaxing-ub?
         (setf (stc-ub-relaxed? constraint) t)
         (setf (stc-original-ub constraint) (upper-bound constraint))
         (setf (upper-bound constraint) relaxed-ub)))

      (relaxable-simple-contingent-temporal-constraint
       (when relaxing-lb?
         (setf (utc-lb-relaxed? constraint) t)
         (setf (utc-original-lb constraint) (lower-bound constraint))
         (setf (lower-bound constraint) relaxed-lb))
       (when relaxing-ub?
         (setf (utc-ub-relaxed? constraint) t)
         (setf (utc-original-ub constraint) (upper-bound constraint))
         (setf (upper-bound constraint) relaxed-ub))))))

(defmethod apply-relaxation ((relaxation chance-constraint-relaxation))

  "Apply the relaxed bounds of this relaxation to the cc constraint"

  (let* ((constraint (ccr-constraint relaxation))
         (relaxing? (ccr-relaxing? relaxation))
         (relaxed-max-prob (ccr-relaxed-max-prob relaxation)))

    (when relaxing?
      (setf (cc-relaxed? constraint) t)
      (setf (cc-original-max-prob constraint) (max-prob-violation constraint))
      (setf (max-prob-violation constraint) relaxed-max-prob))))

(defmethod undo-relaxation ((relaxation continuous-temporal-relaxation))

  "Undo the relaxations for the stc/utc."

  (let* ((constraint (ctr-temporal-constraint relaxation))
         (relaxing-lb? (ctr-relaxing-lb? relaxation))
         (relaxing-ub? (ctr-relaxing-ub? relaxation)))

    (ecase (type-of constraint)
      (relaxable-simple-temporal-constraint
       (when relaxing-lb?
         (setf (stc-lb-relaxed? constraint) nil)
         (setf (lower-bound constraint) (stc-original-lb constraint)))
       (when relaxing-ub?
         (setf (stc-ub-relaxed? constraint) nil)
         (setf (upper-bound constraint) (stc-original-ub constraint))))

      (relaxable-simple-contingent-temporal-constraint
       (when relaxing-lb?
         (setf (utc-lb-relaxed? constraint) nil)
         (setf (lower-bound constraint) (utc-original-lb constraint)))
       (when relaxing-ub?
         (setf (utc-ub-relaxed? constraint) nil)
         (setf (upper-bound constraint) (utc-original-ub constraint)))))))

(defmethod undo-relaxation ((relaxation chance-constraint-relaxation))

  "Undo the relaxations for the cc constraint"

  (let* ((constraint (ccr-constraint relaxation))
         (relaxing? (ccr-relaxing? relaxation)))

    (when relaxing?
      (setf (cc-relaxed? constraint) nil)
      (setf (max-prob-violation constraint) (cc-original-max-prob constraint)))))

(defmethod cs:compute-relaxations-for-candidate
    ((constraint-system temporal-network-constraint-system) cs-candidate &key &allow-other-keys)

  "=> (VALUES t).
   Given a TN constraint system with CS-CANDIDATE
   + Compute and apply the constraint relaxations encoded in cs-candidate."
  ;; (format t "Computing TN relaxations: ~A~%" cs-candidate)

  (let ((continuous-conflicts '())
        (constraint-relaxations '())
        (bound 0))

    (loop for assignment in cs-candidate do
         (let ((variable-name (cs:cs-variable-name (cs:cs-assignment-variable assignment)))
               (assignment-value (cs:cs-assignment-value assignment)))
           (when (and (listp variable-name) (equal assignment-value (intern "OUT")))
             ;; (format t "Continuous conflict ~A~%" variable-name)
             (push variable-name continuous-conflicts))))

    ;; (format t "Continuous conflicts: ~A~%" continuous-conflicts)
    (multiple-value-bind (relaxations cost)
        (compute-continuous-relaxations continuous-conflicts constraint-system)
      (setf constraint-relaxations relaxations)
      ;; (format t "Relaxation Cost: ~A/~A~%" cost relaxations)
      (setf bound (* -1 cost)))

    (loop for relaxation in constraint-relaxations do
       ;; (format t "Relaxing ~A~%" relaxation)
         (apply-relaxation relaxation))

    constraint-relaxations))

;; (defmethod cs:compute-relaxations-for-candidate
;;     ((constraint-system temporal-network-constraint-system) cs-candidate &key &allow-other-keys)

;;   "=> (VALUES t).
;;    Given a TN constraint system with CS-CANDIDATE
;;    + Compute and apply the constraint relaxations encoded in cs-candidate."
;;   (format t "Computing TN relaxations: ~A~%" cs-candidate)

;;   (let ((constraints-suspended '()))

;;     (loop for assignment in cs-candidate do
;;          (let ((variable-name (cs:cs-variable-name (cs:cs-assignment-variable assignment))))
;;            (when (listp variable-name)
;;              (loop for constraint in variable-name do
;;                   (push (first constraint) constraints-suspended)
;;                   (return)))))

;;     (loop for constraint in constraints-suspended do
;;          (cs:remove-active-constraint! constraint-system constraint)
;;          (format t "Removing ~A~%" constraint))

;;     constraints-suspended))

(defmethod cs:undo-constraint-relaxations
    ((constraint-system temporal-network-constraint-system) constraint-relaxations &key &allow-other-keys)

  "=> (VALUES t).
   Given a temporal network with a set of constraint relaxations
   + Undo the changes made by the relaxations to the constraint system .
   See defgeneric for details."

  (loop for relaxation in constraint-relaxations do
       (undo-relaxation relaxation))
    t)

;; (defmethod cs:undo-constraint-relaxations
;;     ((constraint-system temporal-network-constraint-system) constraint-relaxations &key &allow-other-keys)

;;   "=> (VALUES t).
;;    Given a temporal network with a set of constraint relaxations
;;    + Undo the changes made by the relaxations to the constraint system .
;;    See defgeneric for details."

;;     (loop for suspended-constraint in constraint-relaxations do
;;          (cs:insert-active-constraint! constraint-system suspended-constraint))
;;     t)

(defmethod cs:compute-candidate-bound
    ((constraint-system temporal-network-constraint-system) cs-candidate
     &key (full-consistency? nil) &allow-other-keys)

  "=> (VALUES BOUND).
   Given a temporal network with CS-CANDIDATE
   + return the bound on the lowesr relaxation cost (0 is the max, -inf if no relaxation can be found)
   See defgeneric for details."

  (declare (ignore full-consistency?))

  ;; (format t "Computing TN bound: ~A~%" cs-candidate)
  (let ((continuous-conflicts '())
        (bound 0))

    (loop for assignment in cs-candidate do
         (let ((variable-name (cs:cs-variable-name (cs:cs-assignment-variable assignment)))
               (assignment-value (cs:cs-assignment-value assignment)))
           (when (and (listp variable-name) (equal assignment-value (intern "OUT")))
             (push variable-name continuous-conflicts))))

    (multiple-value-bind (relaxations cost)
        (compute-continuous-relaxations continuous-conflicts constraint-system)
      (declare (ignore relaxations))
      ;; (format t "Relaxation Cost: ~A~%" cost)
      (setf bound (* 1 cost)))

    (values bound)))

(defmethod cs:compute-candidate-cost
    ((constraint-system temporal-network-constraint-system) cs-candidate
     &key (full-consistency? nil) &allow-other-keys)

  "=> (VALUES BOUND).
   Given a temporal network with CS-CANDIDATE
   + return the lowesr relaxation cost (0 is the max, -inf if no relaxation can be found)
   See defgeneric for details."

  (declare (ignore full-consistency?))

  ;; (format t "Computing TN cost: ~A~%" cs-candidate)
  (let ((continuous-conflicts '())
        (exact-cost 0))

    (loop for assignment in cs-candidate do
         (let ((variable-name (cs:cs-variable-name (cs:cs-assignment-variable assignment)))
               (assignment-value (cs:cs-assignment-value assignment)))
           (when (and (listp variable-name) (equal assignment-value (intern "OUT")))
             (push variable-name continuous-conflicts))))

    ;; (format t "Continuous conflicts: ~A~%" continuous-conflicts)
    (multiple-value-bind (relaxations cost)
        (compute-continuous-relaxations continuous-conflicts constraint-system)
      (declare (ignore relaxations))
      ;; (format t "Relaxation Cost: ~A/~A~%" cost relaxations)
      (setf exact-cost (* 1 cost)))

    (values exact-cost)))

(defmethod compute-continuous-relaxations (conflicts (constraint-system temporal-network-constraint-system))

  ;; TODO: is it a good idea to separate these into two functions?
  ;; Check if the network is cc-pstn or stnu/stn.
  (if (find :probabilistic-temporal-constraints (features (tn constraint-system)))
      (compute-continuous-relaxations-ccpstn conflicts constraint-system)
      (compute-continuous-relaxations-stnu conflicts constraint-system)))

(defun compute-continuous-relaxations-stnu (conflicts constraint-system)

  "Given a set of conflicts of temporal constraints, compute
and return a set of continuous relaxations that resolve all conflicts"

  (let ((network (tn constraint-system))
        (relaxations (list))
        (cost +inf+)

        ;; Create a map between relaxation variables and variables in the
        ;; num-opt problem.
        (numopt-problem (make-instance 'num-opt:num-opt-problem))
        (stn-2-numopt-var-map (make-bimap :fwd-test #'equal :bwd-test #'equal))
        (numopt-2-stn-var-map (make-bimap :fwd-test #'equal :bwd-test #'equal))
        (numopt-objective (make-instance 'num-opt:linear-objective
                                         :maximize? nil))
        ;; Holding place to create ub-ge-lb-constraints for relaxable utc
        (ub-ge-lb-list (list))

        ;; Use cplex by default
        (solver 'num-opt:cplex-solver)
        (numopt-solver nil)
        (return-status nil))

    ;; (format t "Trying to solve ~A conflicts ~%" (list-length conflicts))

    (when (null conflicts)
      ;; (format t "No conflicts detected. skip and return no relaxation~%")
      (return-from compute-continuous-relaxations-stnu (values relaxations 0)))

    ;; Encode a linear optimization problem
    ;; over the bounds for conflicting resource
    ;; constraints.
    (loop with ncycle-count = 0
       for conflict in conflicts
       do

         (let ((ncycle-constraint nil)
               (rhs 0)
               (var-coefficient (make-hash-table :test #'equalp)))

           (loop for constraint-in-conflict in conflict
              do

              ;; Create variables for the lower and upper bound of the constraint.
                (let* ((var (map-get stn-2-numopt-var-map (list (first constraint-in-conflict) (second constraint-in-conflict))))
                       (constraint-wrapper (first constraint-in-conflict))
                       (constraint (tc constraint-wrapper))
                       (bound (second constraint-in-conflict)))
                  ;; (format t "CIC ~A-~A(~A)~%" (name constraint) (tn::network constraint) bound)
                  (case (type-of constraint)
                    (relaxable-simple-temporal-constraint
                     (when (equal bound :LB)
                       (when (stc-lb-relaxable? constraint)
                         (when (not var)
                           (setf var (make-instance 'num-opt:continuous-variable
                                                    :lower-bound -10000000
                                                    :upper-bound (tn:lower-bound constraint)
                                                    :name (format nil "~A-LB" (name constraint)))))))

                     (when (equal bound :UB)
                       (when (stc-ub-relaxable? constraint)
                         (when (not var)
                           (setf var (make-instance 'num-opt:continuous-variable
                                                    :lower-bound (tn:upper-bound constraint)
                                                    :upper-bound 10000000
                                                    :name (format nil "~A-UB" (name constraint))))))))
                    (relaxable-simple-contingent-temporal-constraint
                     (when (equal bound :LB)
                       (when (utc-lb-relaxable? constraint)
                         (when (not var)
                           (setf var (make-instance 'num-opt:continuous-variable
                                                    :lower-bound (tn:lower-bound constraint)
                                                    :upper-bound (tn:upper-bound constraint)
                                                    :name (format nil "~A-LB" (name constraint))))
                           ;; make sure if both lb and ub of relaxable-utc are relaxable,
                           ;; ub is always bigger than lb
                           (multiple-value-bind (oppo-var exists)
                               (map-get stn-2-numopt-var-map (list constraint-wrapper :UB))
                             (declare (ignore oppo-var))
                             (when exists
                               (push constraint-wrapper ub-ge-lb-list))))))

                     (when (equal bound :UB)
                       (when (utc-ub-relaxable? constraint)
                         (when (not var)
                           (setf var (make-instance 'num-opt:continuous-variable
                                                    :lower-bound (tn:lower-bound constraint)
                                                    :upper-bound (tn:upper-bound constraint)
                                                    :name (format nil "~A-UB" (name constraint))))
                           ;; make sure if both lb and ub of relaxable-utc are relaxable,
                           ;; ub is always bigger than lb
                           (multiple-value-bind (oppo-var exists)
                               (map-get stn-2-numopt-var-map (list constraint-wrapper :LB))
                             (declare (ignore oppo-var))
                             (when exists
                               (push constraint-wrapper ub-ge-lb-list))))))))

                  (if var
                      ;; we do have a relaxable bound to work with
                      (progn
                        (num-opt:add-variable! numopt-problem var)
                        (map-add! stn-2-numopt-var-map (list (first constraint-in-conflict) (second constraint-in-conflict)) var)
                        (map-add! numopt-2-stn-var-map var (list (first constraint-in-conflict) (second constraint-in-conflict)))
                        ;; (format t "Relaxable bound found ~A~%" var)

                        ;; Add the variable to the ncycle constraint
                        (ecase (type-of constraint)
                          (relaxable-simple-temporal-constraint

                           (when (or (equal (third constraint-in-conflict) :-)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :LB)))
                             (if (null (gethash var var-coefficient))
                                 (setf (gethash var var-coefficient) -1)
                                 (setf (gethash var var-coefficient) (- (gethash var var-coefficient) 1)))
                             ;; constraint relaxable, add the lb variable to the objective
                             (setf (num-opt:linear-coefficient numopt-objective var)
                                   (* -1 (stc-lb-cost-ratio constraint))))

                           (when (or (equal (third constraint-in-conflict) :+)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :UB)))
                             (if (null (gethash var var-coefficient))
                                 (setf (gethash var var-coefficient) 1)
                                 (setf (gethash var var-coefficient) (+ (gethash var var-coefficient) 1)))
                             ;; constraint relaxable, add the lb variable to the objective
                             (setf (num-opt:linear-coefficient numopt-objective var)
                                   (* 1 (stc-ub-cost-ratio constraint)))))

                          (relaxable-simple-contingent-temporal-constraint

                           (when (or (equal (third constraint-in-conflict) :+)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :LB)))
                             (if (null (gethash var var-coefficient))
                                 (setf (gethash var var-coefficient) 1)
                                 (setf (gethash var var-coefficient) (+ (gethash var var-coefficient) 1)))
                             ;; constraint relaxable, add the lb variable to the objective
                             (setf (num-opt:linear-coefficient numopt-objective var)
                                   (* 1 (utc-lb-cost-ratio constraint))))

                           (when (or (equal (third constraint-in-conflict) :-)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :UB)))
                             (if (null (gethash var var-coefficient))
                                 (setf (gethash var var-coefficient) -1)
                                 (setf (gethash var var-coefficient) (- (gethash var var-coefficient) 1)))
                             ;; constraint relaxable, add the lb variable to the objective
                             (setf (num-opt:linear-coefficient numopt-objective var)
                                   (* -1 (utc-ub-cost-ratio constraint)))))))

                      ;; we do not have a relaxable bound to work with.
                      ;; in other words, the bound is not relaxable.
                      (progn
                        ;; Move the original value of the bound to the right hand side
                        (ecase (type-of constraint)
                          ((simple-temporal-constraint relaxable-simple-temporal-constraint)
                           (when (or (equal (third constraint-in-conflict) :-)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :LB)))
                             (setf rhs (+ rhs (tn:lower-bound constraint))))
                           (when (or (equal (third constraint-in-conflict) :+)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :UB)))
                             (setf rhs (- rhs (tn:upper-bound constraint)))))
                          ((simple-contingent-temporal-constraint relaxable-simple-contingent-temporal-constraint)
                           (when (or (equal (third constraint-in-conflict) :+)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :LB)))
                             (setf rhs (- rhs (tn:lower-bound constraint))))
                           (when (or (equal (third constraint-in-conflict) :-)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :UB)))
                             (setf rhs (+ rhs (tn:upper-bound constraint))))))))))

           ;; update the rhs of the ncycle-constraint
           (setf ncycle-constraint (make-instance 'num-opt:linear-constraint
                                                  :lower-bound rhs
                                                  :upper-bound 10000000
                                                  :name (format nil "ncycle-constraint-~A" ncycle-count)))
           (setf ncycle-count (+ 1 ncycle-count))

           (loop for var being the hash-keys in var-coefficient using (hash-value coef)
              do (setf (num-opt:linear-coefficient ncycle-constraint var) coef))

           ;; and add it to the num-opt problem
           (num-opt:add-constraint! numopt-problem ncycle-constraint)))

    ;; add all the ub > lb constraints for relaxable utc that has both lb and ub relaxable
    (loop for constraint-wrapper in ub-ge-lb-list
       do (let ((ub-var (map-get stn-2-numopt-var-map (list constraint-wrapper :UB)))
                (lb-var (map-get stn-2-numopt-var-map (list constraint-wrapper :LB)))
                (ub-ge-lb-constraint (make-instance 'num-opt:linear-constraint
                                                  :lower-bound 0
                                                  :upper-bound 10000000
                                                  :name (format nil "ub-ge-lb-constraint-~A" (name (tc constraint-wrapper))))))
            (setf (num-opt:linear-coefficient ub-ge-lb-constraint ub-var) 1)
            (setf (num-opt:linear-coefficient ub-ge-lb-constraint lb-var) -1)
            (num-opt:add-constraint! numopt-problem ub-ge-lb-constraint)))

    (setf (num-opt:objective numopt-problem) numopt-objective)
    ;; (format t "Adding objective: ~A~%" numopt-objective)

    ;; (loop for key being the hash-keys of (num-opt/models::coeff-map numopt-objective)
    ;;    using (hash-value value)
    ;;    do (format t "Objective function coefs: ~S is ~S~%" key value))

    ;; Solve
    (setf numopt-solver (make-instance solver :problem numopt-problem))
    (setf return-status (num-opt:solve numopt-solver))

    ;; (format t "Optimizing...~A~%" return-status)

    ;; Generate relaxations from solutions
    (if (eq num-opt:+status-optimal+ return-status)

        ;; Found solution! Consistent!
        ;; Translate to temporal relaxation

        (progn
          (let* ((numopt-solution (num-opt:get-solution numopt-solver))
                 (objective-value (num-opt:objective-value numopt-solution)))
            (declare (ignore objective-value))
            ;; Now we have a feasible solution, set the cost to zero and start counting.
            (setf cost 0)
            (map-map numopt-2-stn-var-map
                     #'(lambda (numopt-var stn-var)

                         (when (typep stn-var 'list)
                           ;; This is a variable for relaxation
                           (let* ((constraint-wrapper (first stn-var))
                                  (constraint (tc constraint-wrapper))
                                  (bound (second stn-var))
                                  (relaxed-value (num-opt:assignment numopt-solution numopt-var)))

                             ;; (format t "Result: Constraint ~A/ Bound ~A/ Relaxed-Value ~A~%" constraint bound relaxed-value)
                             ;; Create a new resource relaxation object
                             (ecase (type-of constraint)
                               (relaxable-simple-temporal-constraint
                                (when (equal bound :LB)
                                  (setf cost (+ cost (abs (* (stc-lb-cost-ratio constraint) (- (stc-original-lb constraint) relaxed-value)))))
                                  (push (make-instance 'continuous-temporal-relaxation
                                                       :constraint constraint
                                                       :network network
                                                       :relaxing-lb? t
                                                       :relaxed-lb relaxed-value) relaxations))

                                (when (equal bound :UB)
                                  (setf cost (+ cost (abs (* (stc-ub-cost-ratio constraint) (- (stc-original-ub constraint) relaxed-value)))))
                                  (push (make-instance 'continuous-temporal-relaxation
                                                       :constraint constraint
                                                       :network network
                                                       :relaxing-ub? t
                                                       :relaxed-ub relaxed-value) relaxations)))
                               (relaxable-simple-contingent-temporal-constraint
                                (when (equal bound :LB)
                                  (setf cost (+ cost (abs (* (utc-lb-cost-ratio constraint) (- (utc-original-lb constraint) relaxed-value)))))
                                  (push (make-instance 'continuous-temporal-relaxation
                                                       :constraint constraint
                                                       :network network
                                                       :relaxing-lb? t
                                                       :relaxed-lb relaxed-value) relaxations))

                                (when (equal bound :UB)
                                  (setf cost (+ cost (abs (* (utc-ub-cost-ratio constraint) (- (utc-original-ub constraint) relaxed-value)))))
                                  (push (make-instance 'continuous-temporal-relaxation
                                                       :constraint constraint
                                                       :network network
                                                       :relaxing-ub? t
                                                       :relaxed-ub relaxed-value) relaxations)))))))))))
    ;; NOTE: The following lines is before the last   )))
    ;; (format t "Got a relaxation solution with cost ~A!!!~%" cost)
    ;; (setf cost objective-value) ;; this comment is not right

    ;; NOTE: The following lines is before the last   )
    ;; (progn
    ;;   (multiple-value-bind
    ;;         (status rows cols) (num-opt::get-conflicts numopt-solver)
    ;;     (format t "Conflict! No feasible relaxation can be found. return maximal cost~%")
    ;;     (format t "Status:~a ~%row: ~s  ~%column:~a~%" status rows cols)))

    (values relaxations cost)))

(defun compute-continuous-relaxations-ccpstn (conflicts constraint-system)

  "Given a set of conflicts of temporal constraints, compute and return a set of temporal relaxations and cc relaxation that resolve all conflicts"

  (let ((pstn (tn constraint-system))
        (relaxations (list))
        (cost +inf+)

        ;; Create numopt-problem
        ;; Create the variable mapping
        (numopt-problem (make-instance 'num-opt:num-opt-problem))
        (pstn-2-numopt-var-map (make-bimap :fwd-test #'equal :bwd-test #'equal))
        (numopt-var-2-pstn-map (make-bimap :fwd-test #'equal :bwd-test #'equal))
        (numopt-objective nil)
        (initial-point (make-hash-table :test #'equal))
        (objective-expression 0) ;;initialize the objective-expression to be 0

        ;; For nonlinear optimization, use ipopt
        (default-solver 'num-opt:ipopt-solver)
        (numopt-solver nil)
        (return-status nil))

    ;; (format t "Trying to solve ~A conflicts ~%" (list-length conflicts))

    (when (null conflicts)
      ;; (format t "No conflicts detected. skip and return no relaxation~%")
      (return-from compute-continuous-relaxations-ccpstn (values relaxations 0)))

    ;; Encode a nonlinear optimization problem
    ;; over the bounds for conflicting constraints
    ;; and the chance constraints.
    (loop
       with ncycle-count = 0
       with cc-constraint-added = nil
       for conflict in conflicts
       do
         (let ((rhs 0)
               (var-coefficient (make-hash-table :test #'equalp)))

           (loop for constraint-in-conflict in conflict
              do
                (let* ((var (map-get pstn-2-numopt-var-map (list (first constraint-in-conflict) (second constraint-in-conflict))))
                       (constraint-wrapper (first constraint-in-conflict))
                       (constraint (if (typep constraint-wrapper 'tn-constraint-wrapper)
                                       (tc constraint-wrapper)
                                       (cc constraint-wrapper)))
                       (bound (second constraint-in-conflict)))
                  ;; (format t "CIC ~A-~A(~A)~%" (name constraint) (tn::network constraint) bound)

                  ;; First, create variables as necessary, and add them in objective expression
                  (case (type-of constraint)
                    ((relaxable-chance-constraint chance-constraint probabilistic-temporal-constraint)
                     (when (null cc-constraint-added)
                       ;; upon first seeing a chance constraint or ptc constraint, knowing these vars are necessary..
                       ;; (format t "Creating cc-variables, constraint, objective in numopt-problem...~%")
                       ;; obtaining the cc constraint in constraint-system
                       (setf objective-expression
                             (add-cc-var-constraint-objective numopt-problem constraint-system
                                                              pstn-2-numopt-var-map numopt-var-2-pstn-map
                                                              initial-point objective-expression))
                       (setf cc-constraint-added t)))
                    (relaxable-simple-temporal-constraint
                     (when (equal bound :LB)
                       (when (stc-lb-relaxable? constraint)
                         (when (not var)
                           (setf var (make-instance 'num-opt:continuous-variable
                                                    :lower-bound -10000000
                                                    :upper-bound (tn:lower-bound constraint)
                                                    :name (format nil "~A-LB" (name constraint))))
                           ;; (format t "Creating relaxable stn var: ~A~%" var)
                           (num-opt:add-variable! numopt-problem var)
                           (map-add! pstn-2-numopt-var-map (list (first constraint-in-conflict) (second constraint-in-conflict)) var)
                           (map-add! numopt-var-2-pstn-map var (list (first constraint-in-conflict) (second constraint-in-conflict)))
                           (setf (gethash var initial-point) (tn:lower-bound constraint))
                           ;; (setf objective-expression (num-opt:e- objective-expression (num-opt:e* (tn:stc-lb-cost-ratio constraint) var)))
                           ;; let's just use the difference so that there's no need for calculating cost again
                           (setf objective-expression (num-opt:e+ objective-expression (num-opt:e* (stc-lb-cost-ratio constraint) (num-opt:e- (tn:lower-bound constraint) var)))))))

                     (when (equal bound :UB)
                       (when (stc-ub-relaxable? constraint)
                         (when (not var)
                           (setf var (make-instance 'num-opt:continuous-variable
                                                    :lower-bound (tn:upper-bound constraint)
                                                    :upper-bound 10000000
                                                    :name (format nil "~A-UB" (name constraint))))
                           ;; (format t "Creating relaxable stn var: ~A~%" var)
                           (num-opt:add-variable! numopt-problem var)
                           (map-add! pstn-2-numopt-var-map (list (first constraint-in-conflict) (second constraint-in-conflict)) var)
                           (map-add! numopt-var-2-pstn-map var (list (first constraint-in-conflict) (second constraint-in-conflict)))
                           (setf (gethash var initial-point) (tn:upper-bound constraint))
                           ;; (setf objective-expression (num-opt:e+ objective-expression (num-opt:e* (tn:stc-ub-cost-ratio constraint) var)))
                           ;; let's just use the difference so that there's no need for calculating cost again
                           (setf objective-expression (num-opt:e+ objective-expression (num-opt:e* (stc-ub-cost-ratio constraint) (num-opt:e- var (tn:upper-bound constraint))))))))))

                  ;; update the coefficient of the var in the ncycle-constraint
                  (case (type-of constraint)
                    (probabilistic-temporal-constraint
                     (setf var (map-get pstn-2-numopt-var-map (list (first constraint-in-conflict) (second constraint-in-conflict))))
                     (when (or (equal (third constraint-in-conflict) :+)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :LB)))
                       (if (null (gethash var var-coefficient))
                           (setf (gethash var var-coefficient) 1)
                           (setf (gethash var var-coefficient) (+ (gethash var var-coefficient) 1))))

                     (when (or (equal (third constraint-in-conflict) :-)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :UB)))
                       (if (null (gethash var var-coefficient))
                           (setf (gethash var var-coefficient) -1)
                           (setf (gethash var var-coefficient) (- (gethash var var-coefficient) 1)))))
                    ((simple-temporal-constraint relaxable-simple-temporal-constraint)
                     (if var
                         (progn
                           (when (or (equal (third constraint-in-conflict) :-)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :LB)))
                             (if (null (gethash var var-coefficient))
                                 (setf (gethash var var-coefficient) -1)
                                 (setf (gethash var var-coefficient) (- (gethash var var-coefficient) 1))))

                           (when (or (equal (third constraint-in-conflict) :+)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :UB)))
                             (if (null (gethash var var-coefficient))
                                 (setf (gethash var var-coefficient) +1)
                                 (setf (gethash var var-coefficient) (+ (gethash var var-coefficient) 1)))))
                         (progn
                           (when (or (equal (third constraint-in-conflict) :-)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :LB)))
                             (setf rhs (+ rhs (tn:lower-bound constraint))))
                           (when (or (equal (third constraint-in-conflict) :+)
                                     (and (null (third constraint-in-conflict))
                                          (equal bound :UB)))
                             (setf rhs (- rhs (tn:upper-bound constraint))))))))))

           ;; Create a ncycle-constraint for every conflict
           (let ((ncycle-expression 0))
             (maphash #'(lambda (var coef)
                          (setf ncycle-expression (num-opt:e+ ncycle-expression
                                                              (num-opt:e* coef var)))) var-coefficient)
             (num-opt:add-constraint! numopt-problem (make-instance 'num-opt:expression-constraint
                                                                    :expression ncycle-expression
                                                                    :lower-bound rhs
                                                                    :name (format nil "ncycle-constraint-~A" ncycle-count)))
             (setf ncycle-count (+ 1 ncycle-count)))))

    ;; Create objective function
    (setf numopt-objective (make-instance 'num-opt:expression-objective
                                          :maximize? nil
                                          :expression objective-expression))
    (setf (num-opt:objective numopt-problem) numopt-objective)
    ;; (format t "Adding objective: ~A~%" numopt-objective)

    ;; Solve the problem
    (setf numopt-solver (make-instance default-solver :problem numopt-problem))
    (setf return-status (num-opt:solve numopt-solver :initial-point (num-opt:make-hash-table-grounding initial-point)))

    ;; (format t "return status is ~A~%" return-status)

    ;; Generate relaxation from solutions
    (if (eq num-opt:+status-optimal+ return-status)
        (let* ((numopt-solution (num-opt:get-solution numopt-solver))
               (objective-value (num-opt:objective-value numopt-solution)))

          (map-map numopt-var-2-pstn-map
                   #'(lambda (numopt-var pstn-var)
                       (let* ((constraint-wrapper (first pstn-var))
                              (constraint (if (typep constraint-wrapper 'tn-constraint-wrapper)
                                       (tc constraint-wrapper)
                                       (cc constraint-wrapper)))
                              (bound (second pstn-var))
                              (relaxed-value (num-opt:assignment numopt-solution numopt-var)))
                         ;; (format t "Result: Constraint ~A/ Bound ~A/ Relaxed-Value ~A~%" constraint bound relaxed-value)
                         ;; Create a new resource relaxation object
                         (case (type-of constraint)
                           (relaxable-simple-temporal-constraint
                            (when (equal bound :LB)
                              (push (make-instance 'continuous-temporal-relaxation
                                                   :constraint constraint
                                                   :network pstn
                                                   :relaxing-lb? t
                                                   :relaxed-lb relaxed-value) relaxations))

                            (when (equal bound :UB)
                              (setf cost (+ cost (abs (* (stc-ub-cost-ratio constraint) (- (stc-original-ub constraint) relaxed-value)))))
                              (push (make-instance 'continuous-temporal-relaxation
                                                   :constraint constraint
                                                   :network pstn
                                                   :relaxing-ub? t
                                                   :relaxed-ub relaxed-value) relaxations)))
                           (relaxable-chance-constraint
                            (push (make-instance 'chance-constraint-relaxation
                                                 :constraint constraint
                                                 :relaxing? t
                                                 :relaxed-max-prob relaxed-value) relaxations))))))

          ;; (format t "Got a relaxation solution with cost ~A~%" objective-value)
          (setf cost objective-value)))

    (values relaxations cost)))

(defun add-cc-var-constraint-objective (numopt-problem constraint-system
                                        pstn-2-numopt-var-map numopt-var-2-pstn-map
                                        initial-point objective-expression)

  "If the conflicts contain a conflict involving ptc, we need to add cc variables (if relaxable), allocated stnu variables, and the corresponding cc constraint. If cc is relaxable, also need to modify objective function."

  (let* ((pstn (tn constraint-system))
         (c-map (tn/cs:c-map constraint-system))
         (cc-constraint (gethash (tn/cs:cc constraint-system) c-map))
         (cc-var nil))
    ;; Create cc var if relaxable and add to num-opt variables, and add to objective expression
    (let ((constraint (cc cc-constraint)))
      (if (typep constraint 'relaxable-chance-constraint)
          (progn
            (setf cc-var (make-instance 'num-opt:continuous-variable
                                        :lower-bound (max-prob-violation constraint)
                                        :upper-bound 1
                                        :name "cc-var"))
            ;; (format t "Creating relaxable chance-constraint var: ~A~%" cc-var)
            (num-opt:add-variable! numopt-problem cc-var)
            (map-add! pstn-2-numopt-var-map (list cc-constraint nil) cc-var)
            (map-add! numopt-var-2-pstn-map cc-var (list cc-constraint nil))
            (setf (gethash cc-var initial-point) (max-prob-violation constraint))
            (setf objective-expression (num-opt:e+ objective-expression (num-opt:e* (cc-cost-ratio constraint) (num-opt:e- cc-var (max-prob-violation constraint))))))
          (progn
            (setf cc-var (make-instance 'num-opt:continuous-variable
                                        :lower-bound 0
                                        :upper-bound (max-prob-violation constraint)
                                        :name "cc-var"))
            ;; (format t "Creating relaxable chance-constraint var: ~A~%" cc-var)
            (num-opt:add-variable! numopt-problem cc-var)
            (map-add! pstn-2-numopt-var-map (list cc-constraint nil) cc-var)
            (map-add! numopt-var-2-pstn-map cc-var (list cc-constraint nil))
            (setf (gethash cc-var initial-point) (max-prob-violation constraint))
            ;; TODO: the cost here shouldn't matter
            (setf objective-expression (num-opt:e+ objective-expression (num-opt:e* 100000 (num-opt:e- cc-var (max-prob-violation constraint))))))))

    ;; Create lb and ub variables for all ptcs in ccpstn
    (let* ((chance-constraint-expression cc-var))
      (do-constraints (constraint pstn)
        (when (typep constraint 'probabilistic-temporal-constraint)
          (let* ((lb-var (make-instance 'num-opt:continuous-variable
                                        :lower-bound 0
                                        :upper-bound 1000000
                                        :name (format nil "~A-LB" (name constraint))))
                 (ub-var (make-instance 'num-opt:continuous-variable
                                        :lower-bound 0
                                        :upper-bound 1000000
                                        :name (format nil "~A-UB" (name constraint)))))
            (num-opt:add-variable! numopt-problem lb-var)
            (map-add! pstn-2-numopt-var-map (list (gethash constraint c-map) :LB) lb-var)
            (map-add! numopt-var-2-pstn-map lb-var (list (gethash constraint c-map) :LB))
            (setf (gethash lb-var initial-point) 0)
            (num-opt:add-variable! numopt-problem ub-var)
            (map-add! pstn-2-numopt-var-map (list (gethash constraint c-map) :UB) ub-var)
            (map-add! numopt-var-2-pstn-map ub-var (list (gethash constraint c-map) :UB))
            (setf (gethash ub-var initial-point) 1000000)
            ;; add ub > lb constraint
            (num-opt:add-constraint! numopt-problem
                                     (make-instance 'num-opt:expression-constraint
                                                    :expression (num-opt:e- ub-var lb-var)
                                                    :lower-bound 0
                                                    :name (format nil "ub-ge-lb-constraint-~A" (name constraint))))
            ;; add the lb, ub variables in the chance-constraint expression
            (let ((risk-expression (get-risk-expression constraint lb-var ub-var)))
              (setf chance-constraint-expression (num-opt:e- chance-constraint-expression (num-opt:e- 1 risk-expression)))))))
      ;; add the chance-constraint expression to the constraints of the system
      (num-opt:add-constraint! numopt-problem
                               (make-instance 'num-opt:expression-constraint
                                              :expression chance-constraint-expression
                                              :lower-bound 0.000001  ;;TODO: this is temporary to avoid numerical issue
                                              :name (format nil "chance-constraint")))))
  objective-expression)

(defun get-risk-expression (ptc-constraint lb-var ub-var)
  "Get the risk expression from ptc constraint to allocated lb and ub bound"
  (let* ((distribution (make-distribution ptc-constraint)))
    (num-opt:e- (ecdf distribution ub-var)
                (ecdf distribution lb-var))))

(defun make-distribution (probabilistic-duration)
  (apply #'make-instance
         (ecase (dist-type probabilistic-duration)
           (uniform 'probability-distribution:continuous-uniform-distribution)
           (normal 'probability-distribution:normal-distribution))
         (dist-parameters probabilistic-duration)))

(defun ecdf (distribution variable)
  (let ((cdf-expression (num-opt:efun (probability-distribution:cdf distribution) variable))
        (pdf-expression (num-opt:efun (probability-distribution:pdf distribution) variable)))
    (setf (slot-value cdf-expression
                      'num-opt/expressions::derivatives)
          (list pdf-expression))
    cdf-expression))
