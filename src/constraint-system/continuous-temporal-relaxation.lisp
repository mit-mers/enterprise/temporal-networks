;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.


(in-package :tn/cs)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Resource Constraint Relaxation Definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass continuous-temporal-relaxation ()

  ((constraint
    :initarg :constraint
    :accessor ctr-temporal-constraint
    :initform nil
    :documentation
    "The relaxable-STC/UTC object this relaxation is being applied to.")

   (network
    :initarg :network
    :accessor ctr-temporal-network
    :initform nil
    :documentation
    "The resource constraint network that this constraint belongs to.")

   (relaxing-lb?
    :initarg :relaxing-lb?
    :accessor ctr-relaxing-lb?
    :initform nil
    :documentation
    "Indicate if the lower bound of the stc/utc is being relaxed.")

   (relaxed-lb
    :initarg :relaxed-lb
    :accessor ctr-relaxed-lb
    :initform nil
    :documentation
    "The relaxed lb.")

   (relaxing-ub?
    :initarg :relaxing-ub?
    :accessor ctr-relaxing-ub?
    :initform nil
    :documentation
    "Indicate if the upper bound of the stc/utc is being relaxed.")

   (relaxed-ub
    :initarg :relaxed-ub
    :accessor ctr-relaxed-ub
    :initform nil
    :documentation
    "The relaxed ub."))

  (:documentation
   "This object holds information about the relaxation to a simple (contingent) temporal constraint."))


(defmethod print-object ((relaxation continuous-temporal-relaxation) stream)
  (print-unreadable-object (relaxation stream :type t)
    (when (ctr-relaxing-lb? relaxation)
      (with-slots (original-lb) (ctr-temporal-constraint relaxation)
        (format stream "~a (LB: ~A-->~A)" (name (ctr-temporal-constraint relaxation)) original-lb (ctr-relaxed-lb relaxation))))
    (when (ctr-relaxing-ub? relaxation)
      (with-slots (original-ub) (ctr-temporal-constraint relaxation)
        (format stream "~a (UB: ~A-->~A)" (name (ctr-temporal-constraint relaxation)) original-ub (ctr-relaxed-ub relaxation))))))


(defmethod get-cost ((relaxation continuous-temporal-relaxation))

  "Compute the cost of the relaxation."

  (let ((cost 0))

    (with-slots (constraint relaxing-lb? relaxed-lb relaxing-ub? relaxed-ub) relaxation

      (when relaxing-lb?
        (with-slots (lb-cost-ratio original-lb) constraint
          (setf cost (+ cost (abs (* lb-cost-ratio (- original-lb relaxed-lb)))))))

      (when relaxing-ub?
        (with-slots (ub-cost-ratio original-ub) constraint
          (setf cost (+ cost (abs (* ub-cost-ratio (- original-ub relaxed-ub))))))))

    cost))
