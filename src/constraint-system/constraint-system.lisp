;;; -*- indent-tabs-mode:nil; -*-

;;; Copyright (c) 2015, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the MIT MERS Group.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package #:tn/cs)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; variable and state-space definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass tn-event-space (cs:conditional-state-space cs:cs-state-space)
  ((cs:default-variable-type-name :initform :conditional))
  (:documentation "Denotes the space of possible schedules for an STN,
  represented by assignments to event variables."))

(defclass event-wrapper (cs:cs-essential-variable)
  ((event
    :accessor event-wrapper-event))
  (:documentation "Wraps events in the cs-essential-variable class."))

(defmethod cs:variable-type-class ((self tn-event-space) (type (eql :event)))
  'event-wrapper)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; constraint-system definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass temporal-network-constraint-system
          (cs:conditional-constraint-system)
  ((tn
    :accessor tn)
   (cs:state-space-type
    :initform 'tn-event-space)
   (cs:default-constraint-type-name
    :initform 'stc-wrapper)
   (consistency-checker-class
    :accessor consistency-checker-class
    :initarg :consistency-checker-class)
   (consistent-p
    :accessor consistent-p
    :initform nil)
   (cs:last-conflict
    :accessor last-conflict
    :initform nil)
   (c-map
    :accessor c-map
    :initform (make-hash-table :test #'eq)
    :documentation "maps temporal network constraints to their wrapper for
constraint system."))
  (:default-initargs :type :stn))

(defclass cc-pstn-constraint-system (temporal-network-constraint-system)
  ((cc
    :accessor cc)))

(defmethod initialize-instance :after ((self temporal-network-constraint-system)
                                       &rest initargs
                                       &key type name)
  (declare (ignore initargs))
  ;; Look at type and instantiate the correct type of temporal network.
  (ecase type
    (:stn
     (setf (tn self) (make-simple-temporal-network name)))
    (:stnu
     (setf (tn self) (make-simple-temporal-network-with-uncertainty name)))
    (:pstn
     (setf (tn self) (make-probabilistic-simple-temporal-network name))
     (change-class self 'cc-pstn-constraint-system))))

(defmethod cs:constraint-type-class ((self temporal-network-constraint-system)
                                     (type (eql :simple-temporal)))
  'stc-wrapper)

(defmethod cs:constraint-type-class ((self temporal-network-constraint-system)
                                     (type (eql :uncertain-temporal)))
  'utc-wrapper)

(defmethod cs:constraint-type-class ((self temporal-network-constraint-system)
                                     (type (eql :probabilistic-temporal)))
  'ptc-wrapper)

(defmethod cs:constraint-type-class ((self temporal-network-constraint-system)
                                     (type (eql :chance)))
  'chance-constraint-wrapper)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; constraint definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass tn-constraint-wrapper (cs:conditional-constraint)
  ((tc
    :initarg :real-constraint
    :accessor tc)))

(defclass stc-wrapper (tn-constraint-wrapper)
  ())

(defmethod initialize-instance :after ((self stc-wrapper) &rest initargs
                                       &key start end
                                         (lb 0) (ub +inf+)
                                         source name
                                         lb-relaxable? lb-cost-ratio
                                         ub-relaxable? ub-cost-ratio
                                         constraint-system)
  (declare (ignore initargs))
  (if (or lb-relaxable? ub-relaxable?)
      (setf (tc self) (make-instance 'relaxable-simple-temporal-constraint
                                     :from-event start
                                     :to-event end
                                     :lower-bound lb
                                     :upper-bound ub
                                     :lb-relaxable? lb-relaxable?
                                     :lb-cost-ratio lb-cost-ratio
                                     :ub-relaxable? ub-relaxable?
                                     :ub-cost-ratio ub-cost-ratio
                                     :original-lb lb
                                     :original-ub ub
                                     :source source
                                     :name name))
      (setf (tc self) (make-instance 'simple-temporal-constraint
                                     :from-event start
                                     :to-event end
                                     :lower-bound lb
                                     :upper-bound ub
                                     :source source
                                     :name name)))

  (setf (gethash (tc self) (c-map constraint-system)) self))

(defclass utc-wrapper (tn-constraint-wrapper)
  ())

(defmethod initialize-instance :after ((self utc-wrapper) &rest initargs
                                       &key start end
                                         (lb 0) (ub +inf+)
                                         source name
                                         lb-relaxable? lb-cost-ratio
                                         ub-relaxable? ub-cost-ratio
                                         constraint-system)
  (declare (ignore initargs))
  (if (or lb-relaxable? ub-relaxable?)
      (setf (tc self) (make-instance 'relaxable-simple-contingent-temporal-constraint
                                     :from-event start
                                     :to-event end
                                     :lower-bound lb
                                     :upper-bound ub
                                     :lb-relaxable? lb-relaxable?
                                     :lb-cost-ratio lb-cost-ratio
                                     :ub-relaxable? ub-relaxable?
                                     :ub-cost-ratio ub-cost-ratio
                                     :original-lb lb
                                     :original-ub ub
                                     :source source
                                     :name name))
      (setf (tc self) (make-instance 'simple-contingent-temporal-constraint
                                     :from-event start
                                     :to-event end
                                     :lower-bound lb
                                     :upper-bound ub
                                     :source source
                                     :name name)))

  (setf (gethash (tc self) (c-map constraint-system)) self))

(defclass ptc-wrapper (tn-constraint-wrapper)
  ())

(defmethod initialize-instance :after ((self ptc-wrapper) &rest initargs
                                       &key start end
                                            dist-type dist-parameters
                                            source name
                                            constraint-system)
  (declare (ignore initargs))
  (setf (tc self) (make-instance 'probabilistic-temporal-constraint
                                 :from-event start
                                 :to-event end
                                 :dist-type dist-type
                                 :dist-parameters dist-parameters
                                 :source source
                                 :name name))
  (setf (gethash (tc self) (c-map constraint-system)) self))

(defclass chance-constraint-wrapper (cs:conditional-constraint)
  ((cc
    :initarg :real-constraint
    :accessor cc)))

(defmethod initialize-instance :after ((self chance-constraint-wrapper) &rest initargs
                                       &key max-prob-violation
                                         source name
                                         relaxable? cost-ratio
                                         constraint-system)
  (declare (ignore initargs))
  (if relaxable?
      (setf (cc self) (make-instance 'relaxable-chance-constraint
                                     :max-prob-violation max-prob-violation
                                     :relaxable? relaxable?
                                     :cost-ratio cost-ratio
                                     :original-max-prob max-prob-violation
                                     :source source
                                     :name name))
      (setf (cc self) (make-instance 'chance-constraint
                                     :max-prob-violation max-prob-violation
                                     :source source
                                     :name name)))

  (setf (gethash (cc self) (c-map constraint-system)) self))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; temporal-network-constraint-system methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod cs:initialize! ((self temporal-network-constraint-system))
  (setf (default-consistency-checker (tn self)) (consistency-checker-class self)))
;; NOTE: The following follows last )
;; For some reason, itc-v2 doesn't do reset-consistency-checker! need to look
;; into that.

;; (if (default-consistency-checker (tn self))
;;     ;(reset-consistency-checker! (tn self))
;;     (setf (default-consistency-checker (tn self)) (consistency-checker-class self)))

(defmethod cs:insert-active-constraint! ((self temporal-network-constraint-system)
                                         (constraint tn-constraint-wrapper))
  (tn::add-object-to-network! (tn self) (tc constraint)))

(defmethod cs:remove-active-constraint! ((self temporal-network-constraint-system)
                                         (constraint tn-constraint-wrapper))
  (remove-temporal-constraint! (tn self) (tc constraint)))

(defmethod cs:update-consistent! ((self temporal-network-constraint-system) &key &allow-other-keys)
  (multiple-value-bind (consistent? conflict)
      (temporal-network-consistent? (tn self))
    (setf (consistent-p self) consistent?)
    (setf (last-conflict self) conflict))
  (values))

(defmethod cs:update-inconsistent! ((self temporal-network-constraint-system) &key &allow-other-keys)
  (multiple-value-bind (consistent? conflict)
      (temporal-network-consistent? (tn self))
    (setf (consistent-p self) consistent?)
    (setf (last-conflict self) conflict))
  (values))

;; (defmethod cs:extract-conflict ((self temporal-network-constraint-system) &key &allow-other-keys)
;;   (mapcar #'(lambda (x) (gethash (first x) (c-map self)))
;;           (last-conflict self)))

(defmethod cs:extract-conflict ((self temporal-network-constraint-system) &key (hybrid-conflict? nil)  &allow-other-keys)

  ;; for dynamic-controllability-checker, conflict is a conjuction of negative cycles.
  (if (equalp (consistency-checker-class self) 'temporal-controllability:dynamic-controllability-checker)
      (let ((negative-cycles (temporal-controllability:temporal-conflict-cycles (last-conflict self)))
            (total-conflict (list)))
        (loop for negative-cycle in negative-cycles
           do
             (let* ((constraints (temporal-controllability:negative-cycle-constraints negative-cycle))
                    (conflict (mapcar #'(lambda (x) (gethash (first x) (c-map self)))
                                      constraints))
                    (continuous-conflicts '()))

               ;; Convert tn constraints to constraint wrappers
               (loop for constraint in constraints do
                    (push (list (gethash (first constraint) (c-map self)) (second constraint) (third constraint)) continuous-conflicts))

               (if hybrid-conflict?
                   (if total-conflict
                       (nconc total-conflict conflict (list continuous-conflicts))
                       (progn
                         (setf total-conflict (nconc conflict (list continuous-conflicts))))))))
        total-conflict)
      (let ((conflict (mapcar #'(lambda (x) (gethash (first x) (c-map self)))
                              (last-conflict self)))
            (continuous-conflicts '()))
        ;; Convert tn constraints to constraint wrappers
        (loop for constraint in (last-conflict self) do
             (push (list (gethash (first constraint) (c-map self)) (second constraint)) continuous-conflicts))

        (if hybrid-conflict?
            (if conflict
                (nconc conflict (list continuous-conflicts))
                (progn
                  (setf conflict (list continuous-conflicts)))))
        conflict)))

(defmethod cs:lookup-inconsistent? ((self temporal-network-constraint-system))
  (not (consistent-p self)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; cc-pstn-constraint-system methods
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod cs:insert-active-constraint! ((self cc-pstn-constraint-system)
                                         (constraint chance-constraint-wrapper))
  (setf (cc self) (cc constraint))
  (setf (requirements (cc self)) (tn self)))

(defmethod cs:remove-active-constraint! ((self cc-pstn-constraint-system)
                                         (constraint chance-constraint-wrapper))
  (setf (cc self) nil)
  (setf (requirements (cc self)) nil))

(defmethod cs:update-consistent! ((self cc-pstn-constraint-system)
                                  &key &allow-other-keys)
  (multiple-value-bind (consistent? conflict)
      (temporal-network-consistent-with-chance-constraint?
       (tn self) (cc self))
    (setf (consistent-p self) consistent?)
    (setf (last-conflict self) conflict))
  (values))

(defmethod cs:update-inconsistent! ((self cc-pstn-constraint-system)
                                    &key &allow-other-keys)
  (multiple-value-bind (consistent? conflict)
      (temporal-network-consistent-with-chance-constraint?
       (tn self) (cc self))
    (setf (consistent-p self) consistent?)
    (setf (last-conflict self) conflict))
  (values))
