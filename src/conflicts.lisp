;;; -*- indent-tabs-mode:nil; fill-column:80; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

;;; A conflict, as defined at the level of the temporal-networks package, is a
;;; set of temporal constraints that, when taken together, imply the network is
;;; infeasible or uncontrollable (depending on the question being asked).
;;;
;;; A conflict returned by a solver must provide the following API:
;;; + NUMBER-OF-CONSTRAINTS
;;; + CONSTRAINTS-IN-CONFLICT
;;; + REASONS-INVOLVED-IN-CONFLICT
;;;
;;; A constraint description (returned by CONSTRAINTS-IN-CONFLICT) must provide
;;; the following API:
;;;
;;; + CONSTRAINT
;;; + REASONS-INVOLVED-IN-CONFLICT
;;;
;;; If the constraint description is a list, the first element in the list must
;;; be the TEMPORAL-CONSTRAINT-OBJECT. The rest of the list must be a plist
;;; containing, at a minimum, an entry for :REASONS that is a list of keywords
;;; as described in REASONS-INVOLVED-IN-CONFLICT.

(defgeneric number-of-constraints (temporal-conflict)
  (:documentation "=> number

Returns the number of temporal constraints involved in this conflict."))

(defgeneric constraint (temporal-conflict-constraint-description)
  (:documentation
"Return the constraint of a constraint description. Is a no-op on
TEMPORAL-CONSTRAINT class.")
  (:method ((tc temporal-constraint))
    tc)
  (:method ((constraint-description list))
    (first constraint-description)))

(defgeneric constraints-in-conflict (temporal-conflict)
  (:documentation "=> list of constraint descriptions or constraints

Returns a list of temporal constraints involved in the conflict. The returned
list must not be modified in place.

The list may consist of TEMPORAL-CONSTRAINT or other arbitrary objects. To
access the actual constraint, use the generic function CONSTRAINT. The reason a
generic object may be returned is so that implementations of TEMPORAL-CONFLICT
may wrap the reasons in the same object describing the constraint to avoid
hashing or other operations."))

(defgeneric reasons-involved-in-conflict (temporal-conflict temporal-constraint-description)
  (:documentation "=> list of reasons

Given a conflict and a constraint that is in the conflict, returns the reasons
why the constraint is involved in the conflict.

The standard reasons (and applicable classes of constraints) are:

+ :UPPER-BOUND

    + If a simple temporal constraint, this means that the upper bound is part
    of the conflict.

    + If a simple looping constraint, this means that the max number of loops,
    multiplied by the per-duration upper-bound is too small.

+ :LOWER-BOUND

    + If a simple temporal constraint, this means that the lower bound is part
    of the conflict.

    + If a simple looping constraint, this means that the min number of loops,
    multiplied by the per-duration lower-bound is too large.")
  (:method (conflict (constraint-description list))
    (declare (ignore conflict))
    (getf (rest constraint-description) :reasons)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Basic implementation of conflict class
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass basic-temporal-conflict ()
  ((list-of-constraint-descriptions
    :initarg :constraint-descriptions
    :initform nil
    :reader constraints-in-conflict
    :documentation
"A list of constraint descriptions where each constraint description is of the
form: (constraint-object :reasons REASONS).")))

(defmethod number-of-constraints ((temporal-conflict basic-temporal-conflict))
  (length (constraints-in-conflict temporal-conflict)))
