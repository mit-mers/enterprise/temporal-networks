;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BASE CLASS TEMPORAL-EVENT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass temporal-event (temporal-network-member)
  ((incoming-constraints
    :initform nil
    :type list
    :reader incoming-constraints
    :accessor event-incoming-constraints
    :documentation
"A list of TEMPORAL-CONSTRAINT objects with this event as their head event.")
   (outgoing-constraints
    :initform nil
    :type list
    :reader outgoing-constraints
    :accessor event-outgoing-constraints
    :documentation
"A list of TEMPORAL-CONSTRAINT objects with this event as their tail event.")
   (id
    :initform (gensym "EVENT")
    :documentation "Generated IDs start with \"EVENT.\"")
   (contingent-constraint
    :initform nil
    :type (or null contingent-temporal-constraint)
    :accessor contingent-constraint
    :documentation
"If non-NIL, is the single contingent temporal constraint making this event
uncontrollable."))
  (:documentation
"A TEMPORAL-EVENT represents a variable in a temporal constraint network. Its
value is constrained by temporal constraints."))

(defmethod print-object ((event temporal-event) stream)
  (print-unreadable-object (event stream :type t)
    (format stream "~a" (name event))))

(defgeneric controllable? (event)
  (:method ((event temporal-event))
    (null (contingent-constraint event))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Event Specializations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod network-supports-object? and (network (event temporal-event))
  (declare (ignore network))
  t)

(defmethod check-object-unique (network (event temporal-event))
  (with-accessors ((events-by-id events-by-id)) network
    (let ((existing-event (gethash (id event) events-by-id)))
      (when existing-event
        (error 'duplicate-object-id :existing-object existing-event :new-object event :network network)))))

(defmethod perform-add-object-to-network! (network (event temporal-event))
  ;; We know that the object is safe to add, so add it.
  (with-accessors ((events-by-id events-by-id)) network
    (setf (gethash (id event) events-by-id) event)))

(defmethod process-object-added-hooks (network (event temporal-event))
  (fire-handlers (event-added-handlers network) network event))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Hooks.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun add-event-added-handler (network handler &key source provide-source-to-handler?)
  "=> handler reference.

Register a handler to be called when an event is added to NETWORK.

When SOURCE is provided, HANDLER is registered as belonging to SOURCE. Future
calls to REMOVE-EVENT-ADDED-HANDLERS-BY-SOURCE with SOURCE as an argument will
remove HANDLER from the list of handlers called to process this event.
Additionally, HANDLER will no longer be called once SOURCE is garbage
collected.

When PROVIDE-SOURCE-TO-HANDLER? is NIL, HANDLER is called with two arguments:

- The network the event is being added to.
- The event being added.

When PROVIDE-SOURCE-TO-HANDLER? is T, HANDLER is called with three arguments:

- The value of SOURCE.
- The network the event is being added to.
- The event being added.

The returned value is meant to be opaque and should only be used to remove the
handler."
  (add-handler (event-added-handlers network) handler
               :source source :provide-source-to-handler? provide-source-to-handler?))

(defun remove-event-added-handler (network handler)
  "Remove an event added handler. HANDLER must be a reference returned
by ADD-EVENT-ADDED-HANDLER."
  (remove-handler (event-added-handlers network) handler))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Find events.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric find-event (network event-spec &key regex?)
  (:documentation "=> TEMPORAL-EVENT, LIST, or NIL

Find all events in NETWORK that match EVENT-SPEC. Currently supported event
specs are:

- Instance of TEMPORAL-EVENT.
- Event ID (symbol or list).
- ID (string). This searches for all events whose string representation of
  their ID symbol matches the query. May return multiple results (in a list).
  WARNING: searching this way is SLOW.

If only one match is found it is returned directly. If multiple matches are
found, a list of all matches is returned.

Returns NIL if no matching event is found."))

(defmethod find-event ((network temporal-network) (event temporal-event) &key regex?)
  (declare (ignore regex?))
  (when (eq network (network event))
    event))

(defmethod find-event ((network temporal-network) (id symbol) &key regex?)
  (%find-event-by-id network id :regex? regex?))

(defmethod find-event ((network temporal-network) (id list) &key regex?)
  (%find-event-by-id network id :regex? regex?))

(declaim (inline %find-event-by-id))
(defun %find-event-by-id (network id &key regex?)
  (declare (ignore regex?))
  (with-accessors ((events-by-id events-by-id)) network
    ;; Look up the ID. Don't return secondary value as it's not part
    ;; of API that this uses a hash table.
    (nth-value 0 (gethash id events-by-id))))

(defmethod find-event ((network temporal-network) (id string) &key regex?)
  "Searches for events whose string representation of their ID match the query."
  (let ((match-fn (if regex?
                      #'cl-ppcre:scan
                      #'string=))
        (output nil))
    (do-events (event network)
      (when (funcall match-fn id (format nil "~A" (id event)))
        (push event output)))
    (if (rest output)
        output
        (first output))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Ensure events exist.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric ensure-event! (network event-instance-or-id
                           &rest initargs
                           &key type &allow-other-keys)
  (:documentation "=> (values temporal-event event-created?)

Get the TEMPORAL-EVENT object from NETWORK that matches the
EVENT-INSTANCE-OR-ID. If the event doesn't exist, it is created and added to the
network. Meant primarily for internal use.

If the event must be created, an event of type TYPE is created. All arguments
are passed to MAKE-INSTANCE.

The secondary return value is T iff the event had to be created."))

(defmethod ensure-event! ((network temporal-network) (event temporal-event)
                          &rest initargs
                          &key &allow-other-keys)
  (declare (ignore initargs))
  (when (eq (network event)
            network)
    (values event nil)))

(defmethod ensure-event! ((network temporal-network) (id symbol)
                          &rest initargs)
  (apply #'%ensure-event-by-id! network id initargs))

(defmethod ensure-event! ((network temporal-network) (id list)
                          &rest initargs)
  (apply #'%ensure-event-by-id! network id initargs))

(declaim (inline %ensure-event-by-id!))
(defun %ensure-event-by-id! (network id
                             &rest initargs
                             &key (type (default-event-type network))
                               &allow-other-keys)
  ;; Search to see if the event exists.
  (let* ((existing-event (find-event network id)))
    (if existing-event
        ;; The event already exists, return it.
        (values existing-event nil)
        ;; else, make a new event.
        (values (apply #'make-instance type :network network :id id initargs) t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Remove events
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric remove-temporal-event! (network event-spec &key if-constrained &allow-other-keys)
  (:documentation
"=> generalized boolean

Removes all events matching EVENT-SPEC. Returns t if something was removed, nil
otherwise.

IF-CONSTRAINED must be one of two keywords. If :ERROR, an error condition is
signaled if any event matching EVENT-SPEC is involved in a
constraint. If :REMOVE, the constraints that the event is involved in are
removed before this event is removed."))

(defmethod remove-temporal-event! ((network temporal-network)
                                   (te temporal-event)
                                   &key (if-constrained :error)
                                     &allow-other-keys)
  ;; check to see if TE is involved in any constraints.
  (when (and (eql if-constrained :error)
             (or (incoming-constraints te)
                 (outgoing-constraints te)))
    (error "Event ~a is constrained. Unable to remove." te))
  (remove-object-from-network! network te))

(defmethod perform-remove-object-from-network! (network (te temporal-event))
  ;; First, remove all constraints this event is associated with.
  (dolist (tc (incoming-constraints te))
    ;; We call REMOVE-OBJECT directly because we know that each TC is a direct
    ;; reference to a constraint.
    (remove-object-from-network! network tc))
  (dolist (tc (outgoing-constraints te))
    ;; We call REMOVE-OBJECT directly because we know that each TC is a direct
    ;; reference to a constraint.
    (remove-object-from-network! network tc))
  ;; Now remove the object itself.
  (remhash (id te) (events-by-id network))
  (call-next-method)
  (values t te))
