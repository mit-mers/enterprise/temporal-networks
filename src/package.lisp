;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :cl-user)

(uiop:define-package #:temporal-networks
    (:nicknames #:tn)
  (:use #:cl)
  (:export
   ;;; Networks
   #:temporal-network
   #:make-simple-temporal-network
   #:make-simple-temporal-network-with-uncertainty
   #:make-probabilistic-simple-temporal-network
   #:name
   #:id
   #:features
   #:time-domain
   #:define-tn-feature
   #:default-event-type
   #:add-object-to-network!
   #:compute-candidate-bound
   #:compute-candidate-cost

   ;;; Events
   #:temporal-event
   #:find-event
   #:add-event-added-handler
   #:remove-event-added-handler
   #:incoming-constraints
   #:outgoing-constraints
   #:do-events
   #:remove-temporal-event!
   #:contingent-constraint

   ;;; Constraints
   #:temporal-constraint
   #:remove-temporal-constraint
   #:simple-temporal-constraint
   #:simple-contingent-temporal-constraint
   #:probabilistic-temporal-constraint
   #:find-temporal-constraint
   #:remove-temporal-constraint!
   #:add-constraint-added-handler
   #:remove-constraint-added-handler
   #:add-constraint-removed-handler
   #:remove-constraint-removed-handler
   #:add-lower-bound-changed-handler
   #:remove-lower-bound-changed-handler
   #:add-upper-bound-changed-handler
   #:remove-upper-bound-changed-handler
   #:upper-bound
   #:lower-bound
   #:observation-delay
   #:from-event
   #:to-event
   #:source
   #:do-constraints
   #:dist-type
   #:dist-parameters
   #:distribution-parameter
   #:change-distribution!
   #:add-parameter-changed-handler
   #:remove-parameter-changed-handler
   #:add-distribution-changed-handler
   #:remove-distribution-changed-handler
   #:controllable?

      ;;; Consistency checking
   #:temporal-network-consistent?
   #:temporal-network-consistent-with-chance-constraint?
   #:reset-consistency-checker!
   #:default-consistency-checker

   ;;; Conflicts
   #:number-of-constraints
   #:constraint
   #:constraints-in-conflict
   #:reasons-involved-in-conflict
   #:basic-temporal-conflict

   ;;; Adding objects to temporal networks
   #:network-supports-object?
   #:check-network-supports-object
   #:check-object-unique
   #:process-object-added-hooks
   #:perform-add-object-to-network!

   ;;; Removing objects from temporal networks.
   #:process-object-removed-hooks
   #:perform-remove-object-from-network!

   ;;; Helper classes
   #:temporal-network-member

   ;;; Conditions
   #:temporal-network-condition
   #:mismatching-networks
   #:consistency-checker-missing
   #:feature-mismatch
   #:duplicate-object-id

   ;;; Probability Shim
   #:uniform
   #:normal
   #:canonicalize-dist-name
   #:arguments-for-dist-name

   ;; Serialization
   #:dot-visualize

   ;; Copying
   #:copy-into-network))
