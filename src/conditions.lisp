;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

(define-condition temporal-network-condition ()
  ((network
    :initarg :network
    :type temporal-network
    :documentation "The network producing the condition."))
  (:documentation
"Condition that all conditions involving a temporal network inherit from."))

(define-condition mismatching-networks (temporal-network-condition)
  ((object
    :initarg :object
    :type temporal-network-member
    :documentation "The object belonging to another network."))
  (:documentation
"Condition signaled when adding an object to a temporal network that
references an object in a different network."))

(define-condition consistency-checker-missing (temporal-network-condition)
  ()
  (:documentation
"Signals that something has attempted to use the default consistency checker on
a network, but no default is defined."))

(define-condition unsupported-object (temporal-network-condition)
  ((object
    :initarg :object
    :type temporal-network-member
    :documentation "The object that is unsupported by the network."))
  (:documentation
"Used to signal that an object is unsupported by a temporal network."))

(define-condition feature-mismatch (temporal-network-condition)
  ()
  (:documentation
"Signals that a network does not have the correct features to support an
object."))

(define-condition duplicate-object-id (temporal-network-condition)
  ((existing-object
    :initarg :existing-object
    :type temporal-network-member
    :documentation "The object already in the network.")
   (new-object
    :initarg :new-object
    :type temporal-network-member
    :documentation "The object being added to the network."))
  (:documentation
"Condition signaled when an object is being added to a network that already
contains an object with the same ID."))
