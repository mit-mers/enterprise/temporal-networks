(uiop:define-package :mtk-serialization.sxml
    (:use #:cl)
  (:export #:node-has-attributes?
           #:node-has-children?
           #:node-has-value?
           #:node-value
           #:do-node-children
           #:node-name
           #:parse-number
           #:node-children
           #:make-node
           #:*positive-infinity*
           #:*negative-infinity*))

(in-package :mtk-serialization.sxml)

(defvar *positive-infinity* float-features:double-float-positive-infinity
  "The value used to represent infinity when parsing numbers.")

(defvar *negative-infinity* float-features:double-float-negative-infinity
  "The value used to represent infinity when parsing numbers.")

(defun primitive-value-to-string (primitive)
  "=> string representation of PRIMITVE.

Signals an error if PRIMITIVE is not a string, symbol, number, or infinity (from
mtk-utils)."
  (cond
    ((stringp primitive)
     primitive)
    ((eql primitive *positive-infinity*)
     "INF")
    ((eql primitive *negative-infinity*)
     "-INF")
    ((numberp primitive)
     (write-to-string primitive))
    ((symbolp primitive)
     (string primitive))
    (t
     (error "Unable to convert value to string."))))

(defun make-node (name &key attributes-plist value children)
  "=> XML node in SXML format.

"
  (let ((output (list name))
        (attributes nil))
    (when attributes-plist
      (push :@ attributes)
      (loop :for key :in attributes-plist :by #'cddr
         :for val :in (rest attributes-plist) :by #'cddr
         :do (push (list key (primitive-value-to-string val)) attributes))
      (push (nreverse attributes) output))
    (when value
      (push (primitive-value-to-string value) output))
    (when children
      (dolist (child children)
        (push child output)))
    (nreverse output)))

(defun node-has-attributes? (sxml-node)
  "=> boolean

Returns T iff the sxml node has attributes.

This is determined by looking at the second element in the list and seeing if it
is a list that starts with :@"
  (and (listp (second sxml-node))
       (eql :@ (first (second sxml-node)))))

(defun node-has-children? (sxml-node)
  "=> boolean

Returns T iff the sxml node has children.

This is determined by looking at the second or third element in the
list (depending on if the list has attributes) and seeing if it is a list."
  (if (node-children sxml-node)
      t
      nil))

(defun node-has-value? (sxml-node)
  "=> boolean

Returns T iff the sxml node has a value (no children).

This is determined by looking at the second or third element in the
list (depending on if the list has attributes) and seeing if it is a string."
  (if (node-has-attributes? sxml-node)
      (if (>= (length sxml-node) 3)
          (and (stringp (third sxml-node))
               (not (cdddr sxml-node)))
          t)
      (if (>= (length sxml-node) 2)
          (and (stringp (second sxml-node))
               (not (cddr sxml-node)))
          t)))

(defun node-value (sxml-node)
  (assert (node-has-value? sxml-node))
  (if (node-has-attributes? sxml-node)
      (if (>= (length sxml-node) 3)
          (third sxml-node)
          "")
      (if (>= (length sxml-node) 2)
          (second sxml-node)
          "")))

(defun node-children (sxml-node)
  (if (node-has-attributes? sxml-node)
      (remove-if-not #'listp (rest (rest sxml-node)))
      (remove-if-not #'listp (rest sxml-node))))

(defmacro do-node-children ((child sxml-node) &rest body)
  `(dolist (,child (node-children ,sxml-node))
     ,@body))

(defun node-name (sxml-node)
  (first sxml-node))

(defun parse-number (string)
  (cond
    ((equal string "INF")
     *positive-infinity*)
    ((equal string "-INF")
     *negative-infinity*)
    (t
     (parse-number:parse-real-number string))))
