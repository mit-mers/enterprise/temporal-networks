;;; Contains all functionality for serializing and deserializing temporal
;;; networks to/from XML.

(uiop:define-package #:temporal-networks/serialization/xml
    (:nicknames #:tn/serialization/xml)
  (:use :cl
        :mtk-serialization.sxml)
  (:export
   ;; Public API (to be reexported)
   #:parse-network-from-xml-file
   #:parse-network-from-xml-stream
   #:write-network-to-xml-file
   #:write-network-to-xml-stream
   ;; Developer API for parsing (to call, mostly)
   #:parser-state
   #:symbol-id-for-string
   #:parse-network-name
   #:parse-network-features
   #:parse-network-time-domain
   #:parse-node-into-network
   #:parse-node-name
   #:parse-network-time-domain
   #:parse-network-features
   ;; Developer API for writing (to call, mostly)
   #:make-name-node
   #:make-time-domain-node
   #:make-features-node
   #:make-events-node
   #:make-features-node
   #:symbol-for-id-string
   ;; Developer API for parsing (to extend, mostly)
   #:parse-network
   #:%parse-node-into-network
   ;; Developer API for writing (to extend, mostly)
   #:write-network
   #:make-event-node
   #:make-constraint-node
   ;; chance constraint parsing
   #:parse-ccpstp-from-xml-file
   #:parse-ccpstp-from-xml-stream)
  (:import-from :alexandria
                #:ensure-gethash)
  ;; Reexport the utils we used from MTK-SERIALIZATION.SXML (This package will
  ;; probably be moved and/or renamed at some point. We reexport so that
  ;; extenders won't have to worry about that when/if it happens.
  (:reexport :mtk-serialization.sxml))

(in-package :temporal-networks/serialization/xml)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; User API
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun parse-network-from-xml-stream (stream &key id-package)
  "=> temporal network instance

Given a stream, read its contents as a stream of an XML document and parse it into a temporal
network instance.

ID-PACKAGE is a package identifier into which the id symbols will be
interned. If NIL, they are not interned into any package."
  ;; Parse the file into an SXML list.
  (let* ((root (s-xml:parse-xml stream :output-type :sxml))
         ;; Then figure out the name of the root tag.
         (root-name (node-name root)))
    ;; Then dispatch the correct parser based on the root tag.
    (parse-network root-name root :id-package id-package)))

(defun parse-network-from-xml-file (file &key id-package)
  "=> temporal network instance

Given a file, read its contents as an XML document and parse it into a temporal
network instance.

ID-PACKAGE is a package identifier into which the id symbols will be
interned. If NIL, they are not interned into any package."
  ;; Parse the file into an SXML list.
  (with-open-file (stream file :direction :input)
    (parse-network-from-xml-stream stream :id-package id-package)))

(defun write-network-to-xml-stream (network stream)
  "=> boolean

Write NETWORK as an XML document to STREAM. Returns T if the write was successful,
NIL (or condition) if the write was unsuccessful."
  (when stream
    (format stream "~a" (s-xml:print-xml-string (write-network network) :pretty t :input-type :sxml))
    t))

(defun write-network-to-xml-file (network file &key (if-exists :error))
  "=> boolean

Write NETWORK as an XML document to FILE. Returns T if the write was successful,
NIL (or condition) if the write was unsuccessful."
  (with-open-file (f file :direction :output :if-exists if-exists)
    (format f "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
    (write-network-to-xml-stream network f)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Developer API - Parsing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass parser-state ()
  ((id-ht
    :initform (make-hash-table :test #'equal)
    :reader parser-id-ht
    :documentation "A hash table mapping a string to a symbol.

The XML spec refers to objects by ID which are strings. However, we refer to
objects in Lisp by IDs which are symbols. In order to avoid interning a bunch of
symbols into a package and cluttering it (and potentially running out of
memory!), we make the references be uninterned symbols. Since they're
uninterned, we need someway to convert a string to the correct symbol and this
hash table provides that mapping.")
   (id-package
    :initarg :id-package
    :initform nil
    :reader id-package
    :documentation "A package to intern ID symbols into.

If NIL, the symbols are uninterned."))
  (:documentation "Keeps track of state for the parser."))

(declaim (inline make-symbol-for-string))
(defun make-symbol-for-string (id-string parser-state)
  (declare (type simple-string id-string))
  (if (id-package parser-state)
      (intern id-string (id-package parser-state))
      (make-symbol id-string)))

(declaim (inline symbol-for-id-string))
(defun symbol-for-id-string (id-string parser-state)
  "=> values symbol, boolean

The XML format refers to objects by ID. Given a string, this returns the
ID (symbol) of the given type of object.

The second value is T iff the symbol already existed, NIL if a new one was
created.

Assumes that ID-STRING is a simple string. This should always be true if using
S-XML's reader."
  (declare (type simple-string id-string))
  (ensure-gethash id-string (parser-id-ht parser-state)
                  (make-symbol-for-string id-string parser-state)))

(declaim (inline parse-node-into-network))
(defun parse-node-into-network (node network parser-state)
  "Convenience method for calling %PARSE-NODE-INTO-NETWORK which dispatches on
the name of the node."
  (%parse-node-into-network (node-name node) node network parser-state))

(defun parse-network-time-domain (node)
  "Find the time-domain node and intern its value as a keyword."
  (intern (string-upcase
           (node-value (find :|time-domain| node :key #'node-name)))
          :keyword))

(defun parse-network-name (node)
  "Find the name node and parse it as a string."
  (node-value (find :|name| node :key #'node-name)))

(defun parse-network-features (node)
  "Return a list of features as keywords."
  ;; Find the features node.
  (let ((features-parent (find :|features| node :key #'node-name))
        (features nil))
    ;; Then map over the children, collecting features.
    (do-node-children (child features-parent)
      ;; Sanity checks
      (assert (eql :|feature| (node-name child)))
      (push (intern (string-upcase (node-value child)) :keyword) features))
    features))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Developer API - Writing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun make-name-node (network)
  (make-node :|name| :value (tn:name network)))

(defun make-time-domain-node (network)
  (make-node :|time-domain| :value (string-downcase (string (tn:time-domain network)))))

(defun make-features-node (network)
  (let (temp)
    (make-node :|features|
               :children
               (dolist (feature (tn:features network) temp)
                 (push (make-node :|feature| :value (string-downcase (string feature))) temp)))))

(defun make-events-node (network)
  (let (temp)
    (make-node :|events|
               :children
               (tn:do-events (e network temp)
                 (push (make-event-node e network) temp)))))

(defun make-constraints-node (network)
  (let (temp)
    (make-node :|constraints|
               :children
               (tn:do-constraints (c network temp)
                 (push (make-constraint-node c network) temp)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Developer extension points - Parsing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric parse-network (network-name top-level-node &key id-package))

(defmethod parse-network ((network-name (eql :|network|)) top-level-node &key id-package)
  "=> TEMPORAL-NETWORK object

Top level function for parsing a network from an XML file. Must specialize on
the name of the root node in the XML file."
  ;; We are parsing a plain old temporal constraint network. First, determine
  ;; the features this network will have.
  (let* ((top-level-node (node-children top-level-node))
         (features (parse-network-features top-level-node))
         ;; Then, determine the name.
         (name (parse-network-name top-level-node))
         ;; Then the time domain.
         (time-domain (parse-network-time-domain top-level-node))
         ;; Then find the list of events
         (events (find :|events| top-level-node :key #'node-name))
         ;; Then find the list of constraints
         (constraints (find :|constraints| top-level-node :key #'node-name))
         ;; Make a parser state
         (parser-state (make-instance 'parser-state :id-package id-package))
         ;; Last, instantiate the object to fill in.
         (network (make-instance 'tn:temporal-network :features features :time-domain time-domain :name name)))
    ;; Next, instantiate all of the events.
    (do-node-children (event events)
      (parse-node-into-network event network parser-state))
    ;; Then instantiate all of the constraints.
    (do-node-children (constraint constraints)
      (parse-node-into-network constraint network parser-state))
    ;; Return the completed network
    network))

(defgeneric %parse-node-into-network (node-name node network parser-state)
  (:documentation "Called for effect

Parse a node (represented as an SXML list) into NETWORK. Must specialize on
NODE-NAME."))

(defmethod %parse-node-into-network ((node-name (eql :|event|)) event network parser-state)
  (let (name id)
    (do-node-children (child event)
      (ecase (node-name child)
        (:|name|
          (setf name (node-value child)))
        (:|id|
          (setf id (symbol-for-id-string (node-value child) parser-state)))))
    (log:debug "Parsed event " name id)
    (make-instance 'tn:temporal-event
                   :id id
                   :name name
                   :network network))
  (values))


(defmethod %parse-node-into-network ((node-name (eql :|simple-temporal-constraint|)) constraint-node network parser-state)
  (let (id name to-event from-event lower-bound upper-bound)
    (do-node-children (child constraint-node)
      (ecase (node-name child)
        (:|name|
          (setf name (node-value child)))
        (:|id|
          (setf id (symbol-for-id-string (node-value child) parser-state)))
        (:|to-event|
          (setf to-event (symbol-for-id-string (node-value child) parser-state)))
        (:|from-event|
          (setf from-event (symbol-for-id-string (node-value child) parser-state)))
        (:|lower-bound|
          (setf lower-bound (parse-number (node-value child))))
        (:|upper-bound|
          (setf upper-bound (parse-number (node-value child))))))
    (log:debug "Parsed simple temporal constraint " name id to-event from-event lower-bound upper-bound)
    (make-instance 'tn:simple-temporal-constraint
                   :from-event from-event
                   :to-event to-event
                   :id id
                   :name name
                   :network network
                   :upper-bound upper-bound
                   :lower-bound lower-bound))
  (values))

(defmethod %parse-node-into-network ((node-name (eql :|simple-contingent-temporal-constraint|)) constraint-node network parser-state)
  (let (id name to-event from-event lower-bound upper-bound)
    (do-node-children (child constraint-node)
      (ecase (node-name child)
        (:|name|
          (setf name (node-value child)))
        (:|id|
          (setf id (symbol-for-id-string (node-value child) parser-state)))
        (:|to-event|
          (setf to-event (symbol-for-id-string (node-value child) parser-state)))
        (:|from-event|
          (setf from-event (symbol-for-id-string (node-value child) parser-state)))
        (:|lower-bound|
          (setf lower-bound (parse-number (node-value child))))
        (:|upper-bound|
          (setf upper-bound (parse-number (node-value child))))))
    (log:debug "Parsed simple contingent temporal constraint " name id to-event from-event lower-bound upper-bound)
    (make-instance 'tn::simple-contingent-temporal-constraint
                   :from-event from-event
                   :to-event to-event
                   :id id
                   :name name
                   :network network
                   :upper-bound upper-bound
                   :lower-bound lower-bound)))

(defmethod %parse-node-into-network ((node-name (eql :|probabilistic-temporal-constraint|)) constraint-node network parser-state)
  (let (id
        name
        to-event from-event
        (dist-type nil)
        dist-parameters
        dist-parameters-labels
        dist-parameters-values)
    (do-node-children (child constraint-node)
      (ecase (node-name child)
        (:|name|
          (setf name (node-value child)))
        (:|id|
          (setf id (symbol-for-id-string (node-value child) parser-state)))
        (:|to-event|
          (setf to-event (symbol-for-id-string (node-value child) parser-state)))
        (:|from-event|
          (setf from-event (symbol-for-id-string (node-value child) parser-state)))
        (:|type|
          (setf dist-type
                (tn:canonicalize-dist-name (node-value child))))
        (:|parameters|
          (do-node-children (parameter child)
             (ecase (node-name parameter)
               (:|parameter|
                 (push (parse-number (node-value parameter)) dist-parameters-values))))
          (setf dist-parameters-values (nreverse dist-parameters-values)))))
    (log:debug "Parsed probabilistic contingent temporal constraint " name id to-event from-event)
    (if (not dist-type)
        (error "Not a recognised probability distribution."))
    (setf dist-parameters-labels (tn:arguments-for-dist-name dist-type))
    (if (not (eq (length dist-parameters-labels)
                 (length dist-parameters-values)))
        (error "Mismatch in number of distribution parameters."))
    (loop
       :for label :in dist-parameters-labels
       :for value :in dist-parameters-values
       :do
       (push value dist-parameters)
       (push label dist-parameters))
    (make-instance 'tn:probabilistic-temporal-constraint
                   :from-event from-event
                   :to-event to-event
                   :id id
                   :name name
                   :network network
                   :dist-type dist-type
                   :dist-parameters dist-parameters)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Developer Extension Points - Writing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric write-network (network)
  (:documentation "=> Representation of NETWORK as SXML list."))

(defmethod write-network ((network tn:temporal-network))
  "=> Representation of NETWORK as SXML list."
  (make-node :|network|
             :attributes-plist
             (list :|xmlns| "http://mers.csail.mit.edu/temporal-network"
                   :|xsi:schemaLocation| "http://mers.csail.mit.edu/temporal-network http://mers.csail.mit.edu/temporal-network-1.0.xsd"
                   :|xmlns:xsi| "http://www.w3.org/2001/XMLSchema-instance")
             :children
             (list (make-name-node network)
                   (make-time-domain-node network)
                   (make-features-node network)
                   (make-events-node network)
                   (make-constraints-node network))))

(defgeneric make-event-node (event network))

(defmethod make-event-node (event network)
  (declare (ignore network))
  (make-node :|event|
             :children
             (list (make-node :|id| :value (tn:id event))
                   (make-node :|name| :value (tn:name event)))))

(defgeneric make-constraint-node (constraint network))

(defmethod make-constraint-node ((constraint tn:simple-temporal-constraint) network)
  (declare (ignore network))
  (make-node :|simple-temporal-constraint|
             :children
             (list (make-node :|id| :value (tn:id constraint))
                   (make-node :|name| :value (tn:name constraint))
                   (make-node :|to-event| :value (tn:id (tn:to-event constraint)))
                   (make-node :|from-event| :value (tn:id (tn:from-event constraint)))
                   (make-node :|lower-bound| :value (tn:lower-bound constraint))
                   (make-node :|upper-bound| :value (tn:upper-bound constraint)))))

(defmethod make-constraint-node ((constraint tn:simple-contingent-temporal-constraint) network)
  (declare (ignore network))
  (make-node :|simple-contingent-temporal-constraint|
             :children
             (list (make-node :|id| :value (tn:id constraint))
                   (make-node :|name| :value (tn:name constraint))
                   (make-node :|to-event| :value (tn:id (tn:to-event constraint)))
                   (make-node :|from-event| :value (tn:id (tn:from-event constraint)))
                   (make-node :|lower-bound| :value (tn:lower-bound constraint))
                   (make-node :|upper-bound| :value (tn:upper-bound constraint)))))

(defmethod make-constraint-node ((constraint tn:probabilistic-temporal-constraint) network)
  (declare (ignore network))
  (make-node :|probabilistic-temporal-constraint|
             :children
             (list (make-node :|id| :value (tn:id constraint))
                   (make-node :|name| :value (tn:name constraint))
                   (make-node :|to-event| :value (tn:id (tn:to-event constraint)))
                   (make-node :|from-event| :value (tn:id (tn:from-event constraint)))
                   (make-node :|type| :value (symbol-name (tn:dist-type constraint)))
                   (make-node :|parameters| :children
                              (loop :for parameter :in (tn:arguments-for-dist-name (tn:dist-type constraint))
                                 :collecting
                                 (make-node :|parameter|
                                            :value (getf (tn:dist-parameters constraint)
                                                         parameter)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Housekeeping
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; S-XML supports XML namespaces by interning the symbols representing
;;; attribute and node names into specific packages. The "global" XML namespace
;;; corresponds to the :KEYWORD. This next form neuters S-XML's namespace
;;; functionality for the "http://mers.csail.mit.edu/temporal-networks"
;;; namespace by telling S-XML to intern anything claiming to be in this
;;; namespace into the :KEYWWORD package. This could cause issues in the future
;;; if we ever want to embed temporal-networks into other XML files. If this is
;;; the case, we should revisit this decision (and its consequences, especially
;;; for the TPN package).

(eval-when (:load-toplevel :execute)
  (s-xml:register-namespace "http://mers.csail.mit.edu/temporal-network" "tn" :keyword)

  ;;; The following is a temporary measure until Eric reworks the packages.
  (import '(parse-network-from-xml-file
            write-network-to-xml-file)
          :tn)
  (export '(parse-network-from-xml-file
            write-network-to-xml-file)
          :tn))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CCPSTP parsing. This chould probably get moved to another package
;;; eventually.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; This allows parsing a pSTN along with an associated chance-constraint

(defun parse-ccpstp-from-xml-stream (stream &key id-package)
  "=> temporal network instance and list of chance-constraints

Given a stream, read its contents as a stream of an XML document and parse it into a temporal
network instance.

ID-PACKAGE is a package identifier into which the id symbols will be
interned. If NIL, they are not interned into any package."
  ;; Parse the file into an SXML list.
  (let* ((root (s-xml:parse-xml stream :output-type :sxml))
         ;; Then figure out the name of the root tag.
         (root-name (node-name root))
         pstn
         (cc-list nil))
    (assert (eql root-name :|ccpstp|))
    ;; Parse the network. It's OK to hardcode this as a "network" and not a TPN,
    ;; because we're told this is a CCPSTP, which shouldn't be a TPN.
    (setf pstn (parse-network :|network| (find :|network| (node-children root) :key #'node-name)
                              :id-package id-package))
    ;; Now parse the chance constraints.
    (dolist (node (node-children (find :|chance-constraints| (node-children root)
                                       :key #'node-name)))
      (push (parse-chance-constraint node pstn id-package) cc-list))
    (values pstn cc-list)))

(defun parse-ccpstp-from-xml-file (file &key id-package)
  "=> temporal network instance and list of chance-constraints

Given a file, read its contents as an XML document and parse it into a temporal
network instance.

ID-PACKAGE is a package identifier into which the id symbols will be
interned. If NIL, they are not interned into any package."
  (with-open-file (stream file :direction :input)
    (parse-ccpstp-from-xml-stream stream :id-package id-package)))


(defun parse-chance-constraint (cc-node pstn id-package)
  (let (name id constraints probability)
    (do-node-children (child cc-node)
      (ecase (node-name child)
        (:|name|
          (setf name (node-value child)))
        (:|id|
          (setf id (if id-package
                       (intern (node-value child) id-package)
                       (make-symbol (node-value child)))))
        (:|probability|
          (setf probability (parse-number (node-value child))))
        (:|constraints|
          (do-node-children (constraint child)
            (ecase (node-name constraint)
              (:|constraint|
                (push (tn:find-temporal-constraint pstn (node-value constraint))
                      constraints)))))))
    (log:debug "Parsed chance-constraint " name id)
    (list probability constraints id name)))
