(in-package :temporal-networks)

(defclass tn-dot ()
  ((event-label
    :initarg :event-label
    :reader event-label)))

(defmethod cl-dot:graph-object-node ((graph tn-dot) (object temporal-event))
  (make-instance 'cl-dot:node
                 :attributes (list :label (funcall (event-label graph) object)
                                   :shape :circle)))

(defmethod cl-dot:graph-object-points-to ((graph tn-dot) (object temporal-event))
  (loop :for c :in (outgoing-constraints object)
     :collecting (make-instance 'cl-dot:attributed
                                :object (to-event c)
                                :attributes (list :label (format nil "[~A,~A]"
                                                                 (lower-bound c)
                                                                 (upper-bound c))))))

(defun generate-dot-graph (network &key (event-label :id))
  (let ((all-events (loop :for e :being :the :hash-values :in (events-by-id network)
                       :collecting e))
        (tn-dot (make-instance 'tn-dot :event-label (ecase event-label
                                                      (:id
                                                       #'id)
                                                      (:name
                                                       #'name)
                                                      ((nil)
                                                       (constantly "\\N"))))))
    (cl-dot:generate-graph-from-roots tn-dot all-events '(:rankdir "LR"))))

(defun dot-visualize (network outfile &key (format :ps) (event-label :id))
  "Use dot to visualize the network.

EVENT-LABEL may be one of:
+ :id (the id of an event is printed in the node)
+ :name (the name of an event is printed in the node)
+ nil (nodes are semi-randomly assigned in integer and this appears in the node)"
  (let ((graph (generate-dot-graph network :event-label event-label)))
    (cl-dot:dot-graph graph outfile :format format)))
