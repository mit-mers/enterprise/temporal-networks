;;;; This shim is meant to be replaced with a full fledged probability library
;;;; that (at some point) will be part of the mars toolkit. Right now, it merely
;;;; defines the names of recognized probability distributions and their
;;;; arguments.

(in-package :temporal-networks)

(defgeneric canonicalize-dist-name (dist-string)
  (:documentation "When given a name of a distribution as a string, return the
symbol used to represent the distribution's name. Case is ignored.

In the future this may be extended to accept symbols as well (to canonicalize
regardles of which package a symbol is defined in.")
  (:method ((dist-string string))
    (let ((dist-string (string-downcase dist-string)))
      (cond
        ((string= dist-string "uniform")
         'uniform)
        ((string= dist-string "normal")
         'normal)
        (t
         (error "unrecognized distribution: ~a" dist-string))))))

(defgeneric arguments-for-dist-name (dist-symbol)
  (:documentation "When given a distribution name (symbol) returns a list of the
acceptable keywords used as parameters to the distribution. The order they are
returned in is also the order these parameters must appear in an XML serialized
network.

In the future this may be extended to accept strings, in which case it would
canonicalize and return the arguments.")
  (:method ((dist-symbol (eql 'uniform)))
    (list :lower-bound :upper-bound))
  (:method ((dist-symbol (eql 'normal)))
    (list :mean :variance)))

