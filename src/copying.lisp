(in-package #:temporal-networks)

(defgeneric copy-into-network (object network &rest initargs &key type &allow-other-keys)
  (:documentation "=> new-object

Copy OBJECT into NETWORK. OBJECT should be a subclass of
TEMPORAL-NETWORK-MEMBER.

If TYPE is not provided, the new object is the same type as OBJECT. Any
additionaly INITARGS provided will override the corresponding values of OBJECT
in the newly created object.

If TYPE is not a superclass of OBJECT, the missing INITARGS must be provided or
an error will be signalled.")
  (:method ((object temporal-network-member) (network temporal-network) &rest initargs &key type &allow-other-keys)
    ;; If type isn't provided, determine the type of the new obejct.
    (unless type
      (setf type (type-of object)))
    ;; Clear type out of the initargs.
    (when type
      (alexandria:remove-from-plistf initargs :type))
    ;; Call the specialized method for this type.
    (apply #'copy-into-network-by-type object network type initargs)))

(defgeneric copy-into-network-by-type (object network type &rest initargs))

(defmethod copy-into-network-by-type ((object temporal-constraint)
                                      (network temporal-network)
                                      (type (eql 'simple-temporal-constraint))
                                      &rest initargs)
  (make-instance 'simple-temporal-constraint
                 :network network ; This can't be overriden, that would be insanity!
                 :from-event (or (getf initargs :from-event) (id (from-event object)))
                 :to-event (or (getf initargs :to-event) (id (to-event object)))
                 :id (or (getf initargs :id) (id object))
                 :name (or (getf initargs :name) (name object))
                 :upper-bound (or (getf initargs :upper-bound) (upper-bound object))
                 :lower-bound (or (getf initargs :lower-bound) (lower-bound object))))

(defmethod copy-into-network-by-type ((object temporal-constraint)
                                      (network temporal-network)
                                      (type (eql 'simple-contingent-temporal-constraint))
                                      &rest initargs)
  (make-instance 'simple-contingent-temporal-constraint
                 :network network ; This can't be overriden, that would be insanity!
                 :from-event (or (getf initargs :from-event) (id (from-event object)))
                 :to-event (or (getf initargs :to-event) (id (to-event object)))
                 :id (or (getf initargs :id) (id object))
                 :name (or (getf initargs :name) (name object))
                 :upper-bound (or (getf initargs :upper-bound) (upper-bound object))
                 :lower-bound (or (getf initargs :lower-bound) (lower-bound object))))
