;;;; Extensions to the temporal networks package that are enabled if Swank is
;;;; detected in the image.

(in-package :temporal-networks)

(defparameter *swank-dot-file* (namestring (merge-pathnames (uiop:temporary-directory)
                                                            "tn.ps")))

(defparameter *swank-xml-file* (namestring (merge-pathnames (uiop:temporary-directory)
                                                            "tn.xml")))

(defmethod swank:emacs-inspect ((object temporal-network))
  (let ((basic (call-next-method)))
    (append basic
            (list
             (list :action (format nil "[To PostScript]")
                   #'(lambda ()
                       (let ((output (swank::read-from-minibuffer-in-emacs
                                      "Save image to:" *swank-dot-file*)))
                         (unless (zerop (length output))
                           (dot-visualize object output :format :ps :event-label :id)))))
             "  "
             (list :action (format nil "[To XML]")
                   #'(lambda ()
                       (let ((output (swank::read-from-minibuffer-in-emacs
                                      "Serialize network to:"
                                      *swank-xml-file*)))
                         (unless (zerop (length output))
                           (write-network-to-xml-file object output
                                                      :if-exists :supersede)))))))))
