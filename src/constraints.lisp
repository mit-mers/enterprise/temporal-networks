;;; -*- indent-tabs-mode:nil; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BASE CLASS TEMPORAL-CONSTRAINT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass temporal-constraint (temporal-network-member)
  ((from-event
    :initarg :from-event
    :reader from-event
    :documentation
"Commonly referred to as the \"start event,\" but this event does not
necessarily precede the to event.")
   (to-event
    :initarg :to-event
    :reader to-event
    :documentation
"Commonly referred to as the \"end event,\" but this event does not necessarily
come after the from event.")
   (id
    :initform (gensym "CONSTRAINT")
    :documentation "Generated IDs start with \"CONSTRAINT.\"")
   (constraint-removed-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :reader constraint-removed-handlers
    :documentation
"The handlers that will be called if this constraint is removed from its
network."))
  (:documentation
"A temporal constraint between two events in a temporal network. Places a
constraint on the T2-T1, where T2 is the time the to event is executed and T1
is the time the from event is executed."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Finding constraints
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric find-temporal-constraint (network constraint-spec &key regex?)
  (:documentation "=> TEMPORAL-CONSTRAINT, LIST, or NIL

Find all constraints in NETWORK that match CONSTRAINT-SPEC. Currently supported
constraint specs are:

- Instance of TEMPORAL-CONSTRAINT.
- Constraint ID (symbol or list).
- ID (string). This searches for all constraints whose string representation of
  their ID symbol matches the query. May return multiple results (in a list).
  WARNING: searching this way is SLOW.

If only one match is found it is returned directly. If multiple matches are
found, a list of all matches is returned. (Note: The currently supported event
specs will only produce one result. Future extensions will return lists.)

Returns NIL if no matching constraint is found."))

(defmethod find-temporal-constraint ((network temporal-network)
                                     (constraint temporal-constraint) &key regex?)
  (declare (ignore regex?))
  (when (eq network (network constraint))
    constraint))

(defmethod find-temporal-constraint ((network temporal-network) (id symbol) &key regex?)
  (%find-temporal-constraint-by-id network id :regex? regex?))

(defmethod find-temporal-constraint ((network temporal-network) (id list) &key regex?)
  (%find-temporal-constraint-by-id network id :regex? regex?))

(declaim (inline %find-temporal-constraint-by-id))
(defun %find-temporal-constraint-by-id (network id &key regex?)
  (declare (ignore regex?))
  (with-accessors ((constraints-by-id constraints-by-id)) network
    ;; Look up the ID. Don't return secondary value as it's not part
    ;; of API that this uses a hash table.
    (nth-value 0 (gethash id constraints-by-id))))

(defmethod find-temporal-constraint ((network temporal-network) (id string) &key regex?)
  "Searches for constraints whose string representation of their ID match the query."
  (let ((match-fn (if regex?
                      #'cl-ppcre:scan
                      #'string=))
        (output nil))
    (do-constraints (constraint network)
      (when (funcall match-fn id (format nil "~A" (id constraint)))
        (push constraint output)))
    (if (rest output)
        output
        (first output))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Removing constraints
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric remove-temporal-constraint! (network constraint-spec &key &allow-other-keys)
  (:documentation
"=> generalized boolean

Removes all constraints matching CONSTRAINT-SPEC. Returns t if something was
removed, nil otherwise."))

(defmethod remove-temporal-constraint! ((network temporal-network)
                                        (tc temporal-constraint)
                                        &key &allow-other-keys)
  (remove-object-from-network! network tc))

(defmethod remove-temporal-constraint! ((network temporal-network)
                                        constraint-spec
                                        &key &allow-other-keys)
  ;; Find all matching constraints.
  (let ((matches (find-temporal-constraint network constraint-spec)))
    (if matches
        (remove-temporal-constraint! network matches)
        (values nil nil))))

(defmethod perform-remove-object-from-network! ((network temporal-network) (tc temporal-constraint))
  (with-accessors ((constraints-by-id constraints-by-id)) network
    ;; Remove the constraint from the network.
    (remhash (id tc) constraints-by-id)
    ;; Remove the constraint from its events.
    (setf (event-incoming-constraints (to-event tc))
          (delete tc (event-incoming-constraints (to-event tc))
                  :count 1 :test #'eq))
    (setf (event-outgoing-constraints (from-event tc))
          (delete tc (event-outgoing-constraints (from-event tc))
                  :count 1 :test #'eq))
    (call-next-method)
    (values t tc)))

(defmethod process-object-removed-hooks ((network temporal-network) (constraint temporal-constraint))
  ;; Fire the constraint's handler and then the network's handler.
  (fire-handlers (constraint-removed-handlers constraint) network constraint)
  (fire-handlers (constraint-removed-handlers network) network constraint))

(defmethod process-object-removed-hooks :after ((network temporal-network) (constraint temporal-constraint))
  ;; After everything has been fired, remove the handlers.
  (clear-handlers (constraint-removed-handlers constraint)))

(defmethod check-object-unique (network (constraint temporal-constraint))
  (with-accessors ((constraints-by-id constraints-by-id)) network
    (let ((existing-constraint (gethash (id constraint) constraints-by-id)))
      (when existing-constraint
        (error 'duplicate-object-id :existing-object existing-constraint :new-object constraint :network network)))))

(defmethod perform-add-object-to-network! (network (constraint temporal-constraint))
  ;; Resolve the events.
  (let ((to-event (to-event constraint))
        (from-event (from-event constraint)))
    (setf to-event (ensure-event! network to-event))
    (setf from-event (ensure-event! network from-event))
    ;; The only way ensure-event! returns nil is if the argument is a
    ;; temporal-event object belonging to another network.
    (assert to-event
            ()
            'mismatching-networks :network network :object to-event)
    (assert from-event
            ()
            'mismatching-networks :network network :object from-event)
    (setf (slot-value constraint 'to-event) to-event)
    (setf (slot-value constraint 'from-event) from-event))
  ;; Add the constraint to the network's hashmap. and to the
  ;; incoming/outgoing constraints of the events it relates.
  (with-accessors ((constraints-by-id constraints-by-id)) network
    (setf (gethash (id constraint) constraints-by-id) constraint)
    ;; Add the constraint to its scope.
    (push constraint (event-incoming-constraints (to-event constraint)))
    (push constraint (event-outgoing-constraints (from-event constraint)))))

(defmethod process-object-added-hooks (network (constraint temporal-constraint))
  (fire-handlers (constraint-added-handlers network) network constraint))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; duration classes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; These classes are mixed into the base temporal constraint class to
;; provide the useful specific types of a temporal constraint.

(defclass bounded-duration ()
  ((lower-bound
    :initarg :lower-bound
    :accessor lower-bound
    :initform 0)
   (upper-bound
    :initarg :upper-bound
    :accessor upper-bound
    :initform +inf+)
   (lower-bound-changed-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :reader lower-bound-changed-handlers)
   (upper-bound-changed-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :reader upper-bound-changed-handlers))
  (:documentation
"This mixin is used to indicate that some class has a domain bounded by
LOWER-BOUND and UPPER-BOUND with no 'holes' in the domain."))

(defmethod (setf lower-bound) :around (new-value (constraint bounded-duration))
  (let ((old-value (lower-bound constraint)))
    (call-next-method)
    (fire-handlers (lower-bound-changed-handlers constraint) constraint old-value new-value)))

(defmethod (setf upper-bound) :around (new-value (constraint bounded-duration))
  (let ((old-value (upper-bound constraint)))
    (call-next-method)
    (fire-handlers (upper-bound-changed-handlers constraint) constraint old-value new-value)))

(defun add-lower-bound-changed-handler (constraint handler &key source provide-source-to-handler?)
  "=> handler reference.

Add a handler to the hook called when a constraint's lower bound has
changed.

When SOURCE is provided, HANDLER is regiestered as belonging to SOURCE. Future
calls to REMOVE-LOWER-BOUND-CHANGED-HANDLERS-BY-SOURCE with SOURCE as an
argument will remove HANDLER from the list of handlers called to process this
event.  Additionally, HANDLER will no longer be called once SOURCE is garbage
collected.

If SOURCE is provided, it **must not** be reachable from HANDLER. If it is, then
SOURCE will never be garbage collected. Practically, this also means that
CONSTRAINT should not be reachable from HANDLER unless it is known that the
SOURCE is unreachable from the contstraint or its network.

When PROVIDE-SOURCE-TO-HANDLER? is NIL, HANDLER is called with three arguments:

- The constraint being changed.
- The old value of the bound.
- The new value of the bound.

When PROVIDE-SOURCE-TO-HANDLER? is T, HANDLER is called with four arguments:

- The value of SOURCE.
- The constraint being changed.
- The old value of the bound.
- The new value of the bound.

The returned value is meant to be opaque and should only be used to remove the
handler.
"
  (add-handler (lower-bound-changed-handlers constraint) handler
               :source source :provide-source-to-handler? provide-source-to-handler?))

(defun remove-lower-bound-changed-handler (constraint handler)
  "Remove a lower-bound changed handler. HANDLER must be a reference
returned by ADD-LOWER-BOUND-CHANGED-HANDLER."
  (remove-handler (lower-bound-changed-handlers constraint) handler))

(defun add-upper-bound-changed-handler (constraint handler &key source provide-source-to-handler?)
  "=> handler reference.

Add a handler to the hook called when a constraint's upper bound has
changed.

When SOURCE is provided, HANDLER is regiestered as belonging to SOURCE. Future
calls to REMOVE-UPPER-BOUND-CHANGED-HANDLERS-BY-SOURCE with SOURCE as an
argument will remove HANDLER from the list of handlers called to process this
event.  Additionally, HANDLER will no longer be called once SOURCE is garbage
collected.

If SOURCE is provided, it **must not** be reachable from HANDLER. If it is, then
SOURCE will never be garbage collected. Practically, this also means that
CONSTRAINT should not be reachable from HANDLER unless it is known that the
SOURCE is unreachable from the contstraint or its network.

When PROVIDE-SOURCE-TO-HANDLER? is NIL, HANDLER is called with three arguments:

- The constraint being changed.
- The old value of the bound.
- The new value of the bound.

When PROVIDE-SOURCE-TO-HANDLER? is T, HANDLER is called with four arguments:

- The value of SOURCE.
- The constraint being changed.
- The old value of the bound.
- The new value of the bound.

The returned value is meant to be opaque and should only be used to remove the
handler.
"
  (add-handler (upper-bound-changed-handlers constraint) handler
               :source source :provide-source-to-handler? provide-source-to-handler?))

(defun remove-upper-bound-changed-handler (constraint handler)
  "Remove an upper-bound changed handler. HANDLER must be a reference
returned by ADD-UPPER-BOUND-CHANGED-HANDLER."
  (remove-handler (upper-bound-changed-handlers constraint) handler))

(defmethod process-object-removed-hooks :after ((network temporal-network) (duration bounded-duration))
  ;; After everything has been fired, remove the handlers.
  (clear-handlers (lower-bound-changed-handlers duration))
  (clear-handlers (upper-bound-changed-handlers duration)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CLASS SIMPLE-TEMPORAL-CONSTRAINT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass simple-temporal-constraint (temporal-constraint bounded-duration)
  ()
  (:documentation "A TEMPORAL-CONSTRAINT that has a set bounded duration."))

(defmethod network-supports-object? and (network (constraint simple-temporal-constraint))
  "Simple temporal constraints require :SIMPLE-TEMPORAL-CONSTRAINT on the
network's features list."
  (member :simple-temporal-constraints (features network)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Contingent constraints
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass contingent-temporal-constraint (temporal-constraint)
  ()
  (:documentation
"A TEMPORAL-CONSTRAINT whose to event is an uncontrollable event."))

(defmethod network-supports-object? and (network (constraint contingent-temporal-constraint))
  "Contingent temporal constraints require :CONTINGENT-TEMPORAL-CONSTRAINT on
the network's features list. Additionally, the to event must not already be
the to event of another contingent temporal constraint."
  (and (or (not (find-event network (to-event constraint)))
           (controllable? (find-event network (to-event constraint))))
       (member :contingent-temporal-constraints (features network))))

(defmethod perform-add-object-to-network! (network (constraint contingent-temporal-constraint))
  (declare (ignore network))
  ;; Mark the to-event as uncontrollable.
  (setf (contingent-constraint (ensure-event! network (to-event constraint))) constraint)
  (call-next-method))

(defmethod perform-remove-object-from-network! ((network temporal-network) (tc contingent-temporal-constraint))
  ;; Make the to-event controllable.
  (setf (contingent-constraint (to-event tc)) nil)
  (call-next-method))

(defclass simple-contingent-temporal-constraint (contingent-temporal-constraint
                                                 bounded-duration)
  ((observation-delay
    :initarg :observation-delay
    :accessor observation-delay
    :initform 0))
  (:documentation
"A contingent temporal constraint with a set bounded duration."))

(defmethod network-supports-object? and (network (constraint simple-contingent-temporal-constraint))
  (member :simple-contingent-temporal-constraints (features network)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Probabilistic durations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass probabilistic-duration ()
  ((type
    :initarg :dist-type
    :reader dist-type
    :accessor %dist-type
    :initform 'uniform
    :documentation "The type of distribution. Change with `change-distribution!`")
   (parameters
    :initarg :dist-parameters
    :reader dist-parameters
    :accessor %dist-parameters
    :initform (list :lower-bound 0 :upper-bound 1)
    :documentation "The parameters of the distribution. Modify with `(setf distribution-parameter)`")
   (distribution-changed-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :reader distribution-changed-handlers)
   (parameter-changed-handlers
    :initform (make-event-handlers)
    :type event-handlers
    :reader parameter-changed-handlers))
  (:documentation
"This contains information about a probabilistic duration, giving the type of
distribution and the parameters."))

(defclass probabilistic-temporal-constraint (contingent-temporal-constraint
                                             probabilistic-duration)
  ()
  (:documentation
"A contingent temporal constraint with a probability distribution."))

(defmethod network-supports-object? and (network (constraint probabilistic-temporal-constraint))
  (member :probabilistic-temporal-constraints (features network)))

(defun add-parameter-changed-handler (constraint handler &key source provide-source-to-handler?)
  "=> handler reference.

Add a handler to the hook called when a paramter of a probabilistic constraint
has changed. The returned value is meant to be opaque and is subject to change
at any time."
  (add-handler (parameter-changed-handlers constraint) handler
               :source source :provide-source-to-handler? provide-source-to-handler?))

(defun remove-parameter-changed-handler (constraint handler)
  "Remove a probabilistic distribution parameter changed handler. HANDLER must
be a reference returned by ADD-LOWER-BOUND-CHANGED-HANDLER."
  (remove-handler (parameter-changed-handlers constraint) handler))

(defun add-distribution-changed-handler (constraint handler &key source provide-source-to-handler?)
  "=> handler reference.

Add a handler to the hook called when the distribution type of a probabilistic
constraint has changed. The returned value is meant to be opaque and is subject
to change at any time."
  (add-handler (distribution-changed-handlers constraint) handler
               :source source :provide-source-to-handler? provide-source-to-handler?))

(defun remove-distribution-changed-handler (constraint handler)
  "Remove a probabilistic distribution changed handler. HANDLER must be a
reference returned by ADD-LOWER-BOUND-CHANGED-HANDLER."
  (remove-handler (distribution-changed-handlers constraint) handler))

(declaim (inline distribution-parameter))
(defun distribution-parameter (probabilistic-constraint parameter &optional default)
  "=> value-of-parameter"
  (getf (dist-parameters probabilistic-constraint) parameter default))

(defun (setf distribution-parameter) (new-value probabilistic-constraint parameter
                                      &optional default)
  (declare (ignore default))
  (let* ((no-exist (gensym))
         (old-value (distribution-parameter probabilistic-constraint parameter no-exist)))
    ;; For now, be conservative and throw an error if the parameter does not
    ;; already exist.
    (when (eq no-exist old-value)
      (error t "Parameter ~A does not exist in this distribution." parameter))
    ;; Set the new value.
    (setf (getf (%dist-parameters probabilistic-constraint) parameter) new-value)
    ;; Fire the handlers.
    (fire-handlers (parameter-changed-handlers probabilistic-constraint)
                   probabilistic-constraint parameter old-value new-value))
  new-value)

(defun change-distribution! (probabilistic-constraint new-type new-parameters)
  "Atomically change the distribution type and parameters of a probabilistic
constraint."
  (let ((old-type (dist-type probabilistic-constraint))
        (old-parameters (dist-type probabilistic-constraint)))
    (setf (%dist-type probabilistic-constraint) new-type)
    (setf (%dist-parameters probabilistic-constraint) new-parameters)
    (fire-handlers (distribution-changed-handlers probabilistic-constraint)
                   probabilistic-constraint
                   old-type old-parameters
                   new-type new-parameters)
    (values)))

(defmethod process-object-removed-hooks :after ((network temporal-network) (duration probabilistic-duration))
  ;; After everything has been fired, remove the handlers.
  (clear-handlers (parameter-changed-handlers duration))
  (clear-handlers (distribution-changed-handlers duration)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Printing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod print-object ((x temporal-constraint) stream)
  (with-slots (to-event from-event network) x
    (print-unreadable-object (x stream :type t)
      (if network
          (format stream "from: ~a, to: ~a, network: ~a"
                  from-event to-event (name network))
          (format stream "from: ~a, to: ~a"
                  from-event to-event)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Constraint Handlers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun add-constraint-added-handler (network handler
                                     &key source provide-source-to-handler?)
  "=> handler reference.

Register a handler to be called when a constraint is added to NETWORK.

When SOURCE is provided, HANDLER is regiestered as belonging to SOURCE. Future
calls to REMOVE-CONSTRAINT-ADDED-HANDLERS-BY-SOURCE with SOURCE as an argument
will remove HANDLER from the list of handlers called to process this event.
Additionally, HANDLER will no longer be called once SOURCE is garbage
collected.

If SOURCE is provided, it **must not** be reachable from HANDLER. If it is, then
SOURCE will never be garbage collected. Practically, this also means that
NETWORK should not be reachable from HANDLER unless it is known that the SOURCE
is unreachable from the network.

When PROVIDE-SOURCE-TO-HANDLER? is NIL, HANDLER is called with two arguments:

- The network the constraint is being added to.
- The constraint being added.

When PROVIDE-SOURCE-TO-HANDLER? is T, HANDLER is called with three arguments:

- The value of SOURCE.
- The network the constraint is being added to.
- The constraint being added.

The returned value is meant to be opaque and should only be used to remove the
handler."
  (add-handler (constraint-added-handlers network) handler
               :source source :provide-source-to-handler? provide-source-to-handler?))

(defgeneric add-constraint-removed-handler (network-or-constraint handler
                                            &key source provide-source-to-handler?)
  (:documentation
   "=> handler reference.

Register a handler to be called when a CONSTRAINT is removed from its network.

When SOURCE is provided, HANDLER is regiestered as belonging to SOURCE. Future
calls to REMOVE-CONSTRAINT-REMOVED-HANDLERS-BY-SOURCE with SOURCE as an argument
will remove HANDLER from the list of handlers called to process this event.
Additionally, HANDLER will no longer be called once SOURCE is garbage
collected.

If SOURCE is provided, it **must not** be reachable from HANDLER. If it is, then
SOURCE will never be garbage collected. Practically, this also means that
NETWORK-OR-CONSTRAINT should not be reachable from HANDLER unless it is known
that the SOURCE is unreachable from the contstraint or its network.

When PROVIDE-SOURCE-TO-HANDLER? is NIL, HANDLER is called with two arguments:

- The network the constraint is being removed from.
- The constraint being removed.

When PROVIDE-SOURCE-TO-HANDLER? is T, HANDLER is called with three arguments:

- The value of SOURCE.
- The network the constraint is being removed from.
- The constraint being removed.

When HANDLER is called, CONSTRAINT is guaranteed to be unreachable from its
network.

The returned value is meant to be opaque and should only be used to remove the
handler.")
  (:method ((constraint temporal-constraint) handler &key source provide-source-to-handler?)
    (add-handler (constraint-removed-handlers constraint) handler
                 :source source :provide-source-to-handler? provide-source-to-handler?))
  (:method ((network temporal-network) handler &key source provide-source-to-handler?)
    (add-handler (constraint-removed-handlers network) handler
                 :source source :provide-source-to-handler? provide-source-to-handler?)))

(defun remove-constraint-added-handler (network handler)
  "Remove a constraint added handler. HANDLER must be a reference returned
by ADD-CONSTRAINT-ADDED-HANDLER."
  (remove-handler (constraint-added-handlers network) handler))

(defgeneric remove-constraint-removed-handler (network-or-constraint handler)
  (:documentation
"Remove a constraint added handler. HANDLER must be a reference returned by
ADD-CONSTRAINT-REMOVED-HANDLER.")
  (:method ((constraint temporal-constraint) handler)
    (remove-handler (constraint-removed-handlers constraint) handler))
  (:method ((network temporal-network) handler)
    (remove-handler (constraint-removed-handlers network) handler)))
