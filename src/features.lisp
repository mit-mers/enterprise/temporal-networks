;;; -*- indent-tabs-mode:nil; fill-column:80; -*-

;;;; Copyright (c) 2014 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :temporal-networks)

;;; Contains features of temporal networks and their dependencies.

(defvar *tn-features* (make-hash-table)
"Hash table that maps feature names to dependency structures. Do not access
directly!")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Defining Features
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro define-tn-feature (name &key requires)
  `(eval-when (:load-toplevel :execute)
     (setf (gethash ,name *tn-features*) ,requires)))

(define-tn-feature :simple-temporal-constraints)

(define-tn-feature :contingent-temporal-constraints)

(define-tn-feature :simple-contingent-temporal-constraints
    :requires :contingent-temporal-constraints)

(define-tn-feature :probabilistic-temporal-constraints
    :requires :contingent-temporal-constraints)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Comparing Features
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun resolve-feature-dependencies (features)
  "Given a list a features, return a list consisting of the given features and
their required dependencies. Shares structure with FEATURES."
  (let ((resolved-list features))
    (labels ((resolve-dependencies (feature)
               (multiple-value-bind (dependency valid-feature?)
                   (gethash feature *tn-features*)
                 (unless valid-feature?
                   (error (format nil "unknown feature: ~a" feature)))
                 (when dependency
                   (unless (member dependency resolved-list)
                     (push dependency resolved-list)
                     (resolve-dependencies dependency))))))
      (mapc #'resolve-dependencies features))
    resolved-list))

(defun process-feature-statement (statement form)
  (case statement
    (:require
     `(lambda (features)
         (member ,form features)))
    (:disallow
     `(lambda (features)
         (not (member ,form features))))
    (t
     (error "Unknown statement in feature statement."))))

(defmacro check-network-features (network &rest statements)
  (let ((features-name (gentemp))
        (statement-dispatcher-name (gentemp))
        (lambdas (loop for statement in statements
                    collecting (process-feature-statement (first statement)
                                                          (second statement)))))
    `(let ((,features-name (features ,network)))
       (flet ((,statement-dispatcher-name (fun)
                (funcall fun ,features-name)))
         (unless (every #',statement-dispatcher-name ',lambdas)
           (error "Network does not satisfy features."))
         (values)))))

;; (check-network-features network :require)

;; (defstruct features-combo
;;   required-function
;;   disallow-function
;;   allow-function)

;; (defun make-required-function (description)
;;   (labels ((expand-functional-form (form)
;;              ()
;; )))

;;   #'(lambda (features)


;; )
;; )
;; (defmacro features-combo (&key require disallow allow)

;; )

;; (defun features-compatible? (features features-combo)
;;   (every features-combo features))

;; (check-network-features network (features-combo :require :simple-temporal-constraints
;;                                                 :disallow :contingent-temporal-constraints
;;                                                 ))
