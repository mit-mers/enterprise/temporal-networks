# Temporal Networks #

The high level goals of this system are:

1. Provide a set of models for temporal constraints and temporal constraint
   networks.

2. Provide hooks for solvers or UIs to be notified of changes to the constraints
   or network.

3. Provide a set of minimal interface functions (with no implementation) for
   questions commonly asked of temporal networks (such as if it is consistent.)

## Quickstart ##

Here's how to model a simple STN with three events: a, b, and c:

```lisp
(in-package :cl-user)
(use-package :temporal-networks)
(defparameter *stn* (make-simple-temporal-network "example"))
(make-instance 'simple-temporal-constraint :from-event :a :to-event :b
               :network *stn* :id 'stc1
               :lower-bound 10)
(make-instance 'simple-temporal-constraint :from-event :b :to-event :c
               :network *stn* :id 'stc2
               :lower-bound 0 :upper-bound 10)
```

## Constraint Types ##

Every constraint must be provided a `:to-event` and a `:from-event`. These may
be either IDs of events or actual event objects.

There are two general types of constraints: contingent and non-contingent. When
a contingent constraint is inserted into a network, it marks its to-event as a
contingent event. An error is signaled if two or more contingent constraints
share a to-event.

The following constraint types and arguments are currently supported.

+ simple-temporal-constraint (non-contingent)
    + :lower-bound (default 0)
    + :upper-bound (default :infinity)
+ simple-contingent-temporal-constraint (contingent)
    + :lower-bound (default 0)
    + :upper-bound (default :infinity)
+ probabilistic-temporal-constraint (contingent)
    + :dist-type (one of: 'uniform or 'normal. default: 'uniform)
    + :dist-parameters (default: '(:lower-bound 0 :upper-bound 1))

## Hooks ##

Temporal networks are frequently not static. Constraints are added and removed
by an operator or algorithmically, constraint bounds can change, etc. In order
to notify solvers or visualizers of these changes, temporal-networks provides an
interface where interested code can request to be notified of changes. The
following functions respond to temporal networks:

+ `add-constraint-added-handler network handler &key source provide-source-to-handler?`
+ `add-constraint-removed-handler network handler &key source provide-source-to-handler?`

These functions respond to every constraint:

+ `add-constraint-removed-handler constraint handler &key source provide-source-to-handler?`

These functions respond to simple constraints:

+ `add-upper-bound-changed-handler constraint handler &key source provide-source-to-handler?`
+ `add-lower-bound-changed-handler constraint handler &key source provide-source-to-handler?`

## Common Solver Interface ##

To ease the use of the temporal-networks system as a front end to a consistency
checker, you can set the default consistency checker of an temporal network using:

+ `(setf (default-consistency-checker network))`

If the argument given to the `setf` is a symbol, it will automatically
instantiate the object with the same name and pass the network as an argument.

Additionally, temporal-networks defines the generic function
`temporal-network-consistent` as well as `reset-consistency-checker!`. If these
functions are called on a temporal-network object, the call is simply forwarded
to the default consistency checker if set.

## Debugging ##

### XML (De)Serialization ###

A temporal network can be save to an XML file by using"

`parse-network-from-xml-file file &key id-package`
+ If `id-package` is non-NIL, ID symbols are interned into that package,
  otherwise they are uninterned.

A temporal network can be written to an XML file by using:

`write-network-to-xml-file network file &key (if-exists :error)`

### Visualization ###

A dot visualization of a temporal network can be created using:

`dot-visualize network outfile &key (format :ps) (event-label :id)`
+ `format` is one of (`:ps` `:pdf` `:png`)
+ `event-label` is one of:
    + `:id` - events are labeled with their ID.
    + `:name` - events are labeled with their name.
    + `nil` - events are labeled with a semi-random integer.

### SLIME Integration ###

If SLIME is loaded into the image when temporal-networks is loaded, it will
enable extra debugging features in conjunction with SLIME's inspector. Namely,
two new actions will appear for any temporal network opened in the inspector
that allow you to save the network to a file or create a visualization of the
network.

## Future Work ##

+ Simplify event API by removing functions to register for every event and
  providing a single function that specializes on an argument specifying the
  event of interest.
+ Specify a model and API for schedules/scheduling?

## Oddities ##

We plan to address these in future versions:

+ When saving to an XML file, an error will occur if the ID of some object is
  not a symbol.

## OpSAT Integration ##

To facilitate using solvers written for temporal networks with OpSAT, the
`:temporal-networks/constraint-system` system provides a wrapper around a
temporal-network that follows the interface described by constraint-system.

When calling `cs:new-constraint` on the wrapper, specify the type of temporal
constraint using the `:type` keyword. The supported types for constraint system
are:

+ `:stc`
